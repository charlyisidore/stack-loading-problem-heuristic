Stack Loading Problem Heuristic
===============================

See: [Solving the Problem of Stacking Goods: Mathematical Model, Heuristics and a Case Study in Container Stacking in Ports](https://ieeexplore.ieee.org/document/9328429)

This C++ program solves the *stack loading problem* to minimize the number of
blocking items with stacking constraints, using:

- A [heuristic framework](https://ieeexplore.ieee.org/document/9328429)
- The CPLEX MIP solver
- The GUROBI MIP solver
- The SCIP MIP solver


# Instances

Our random instances are in the `instances` folder.
Instances are [JSON](https://www.json.org/) files.
The root element is an object having the following fields:

- `n_items`: number of items
- `n_stacks`: number of stacks
- `capacity`: stack capacity (maximum height)
- `departures`: (optional) array of retrieval times
- `blocking_matrix`: (optional) a matrix which values are 1 if $`r_{ij} = 1`$, 0 otherwise
- `weights`: (optional) array of weights
- `conflict_matrix`: (optional) a matrix in which values are 1 if $`s_{ij} = 0`$, 0 otherwise

Either `departures` or `blocking_matrix` must be specified.
If `weights` and `conflict_matrix` are not specified, items have zero weight.


# Get started

This program requires `cmake` and the `nlohmann-json` library installed (`nlohmann-json3-dev` package on Ubuntu distributions).
The `cplex`, `gurobi` and `scip` libraries are optional, but enable a lot of features.


## Compile

Create a `build` directory and run `cmake`.

```bash
mkdir build
cd build/
cmake ..
```

If `cplex` is not detected, you can specify its directory in the variable `CPLEX_DIR` (e.g. `/opt/ibm/ILOG/CPLEX_Studio129/`).
If `gurobi` is not detected, you can specify its directory in the variable `GUROBI_DIR` (e.g. `/opt/gurobi902/linux64`).

Compile:

```bash
make
```


## Run

To read the manual, run:

```bash
./stacking --help
```

The `stdout` output of the program contains JSON data.

The fields for `heuristic` are:

- `seed`: Random seed that can be given to the program to reproduce the same results
- `is_feasible`: 1 if the generated solution is feasible, 0 otherwise
- `n_blocking_items`: Number of blocking items
- `n_blocked_items`: Number of blocked items
- `n_blocking_items_construct`: Number of blocking items after construction
- `n_blocked_items_construct`: Number of blocked items after construction
- `construct_time`: Construction phase execution time
- `local_search_time`: Local search execution time
- `total_time`: Total execution time
- `n_repairs`: Number of repair attempts executed
- `n_1_opt_moves`: Number of 1-opt moves executed
- `n_2_opt_moves`: Number of 2-opt moves executed

The fields for `cplex`, `gurobi` and `scip` are:

- `is_feasible`: 1 if the generated solution is feasible, 0 otherwise
- `is_optimal`: 1 if the generated solution is optimal, 0 otherwise
- `n_blocking_items`: Number of blocking items
- `n_blocked_items`: Number of blocked items
- `total_time`: Total execution time
