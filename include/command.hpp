/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMMAND_HPP
#define COMMAND_HPP

#include "dopt.hpp"
#include "stacking_model.hpp"
#include "stacking_solution.hpp"
#include <iostream>
#include <nlohmann/json.hpp>
#include <string>

/**
 * Base class for command line runners.
 */
class Command
{
public:
    /**
     * Destructor.
     */
    virtual ~Command() = default;

    /**
     * Read command line options.
     */
    virtual void read_options(const dopt::Map &) {}

    /**
     * Print command line options.
     */
    virtual void print_options(std::ostream &) const {}

    /**
     * Initialize the solver.
     */
    virtual void initialize(const StackingModel &) {}

    /**
     * Run the solver.
     */
    virtual void solve() {}

    /**
     * Build the solution.
     */
    virtual StackingSolution finalize()
    {
        return StackingSolution();
    }

    /**
     * Set the output data.
     */
    virtual void output(nlohmann::json &, const StackingSolution &) const {}
};

#endif