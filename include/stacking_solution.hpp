/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STACKING_SOLUTION_HPP
#define STACKING_SOLUTION_HPP

#include "stacking_model.hpp"
#include <functional>
#include <iostream>
#include <string>
#include <vector>

/**
 * Solution for the Stacking Problem.
 */
class StackingSolution
{
public:
    /**
     * Model type definition.
     */
    typedef StackingModel Model;

    /**
     * Default constructor.
     */
    StackingSolution() = default;

    /**
     * Constructor.
     */
    StackingSolution(const Model &model);

    /**
     * Copy constructor.
     */
    StackingSolution(const StackingSolution &) = default;

    /**
     * Move constructor.
     */
    StackingSolution(StackingSolution &&) = default;

    /**
     * Copy assignment operator.
     */
    StackingSolution &operator=(const StackingSolution &) = default;

    /**
     * Move assignment operator.
     */
    StackingSolution &operator=(StackingSolution &&) = default;

    /**
     * Initialize the solution.
     */
    void initialize(const Model &model);

    /**
     * Get the number of items.
     */
    std::size_t n_items() const;

    /**
     * Get the number of stacks.
     */
    std::size_t n_stacks() const;

    /**
     * Get the number of blocking items.
     */
    std::size_t n_blocking_items() const;

    /**
     * Get the number of blocked items.
     */
    std::size_t n_blocked_items() const;

    /**
     * Get the number of blocked items.
     */
    std::size_t n_used_stacks() const;

    /**
     * Get the number of violating items.
     */
    std::size_t n_violating_items(const Model &model) const;

    /**
     * Get the stack assigned to an item.
     */
    std::size_t assignment(std::size_t i) const;

    /**
     * Get all the assignments.
     */
    const std::vector<std::size_t> &assignments() const;

    /**
     * Count the number of items blocking a given item.
     */
    std::size_t n_items_blocking(std::size_t i) const;

    /**
     * Count the number of items blocked by a given item.
     */
    std::size_t n_items_blocked_by(std::size_t i) const;

    /**
     * Get the height of a stack.
     */
    std::size_t height(std::size_t k) const;

    /**
     * Check if the solution is feasible.
     */
    bool is_feasible(const Model &model) const;

    /**
     * Find items that belongs to given stack.
     */
    std::vector<std::size_t> items(std::size_t k) const;

    /**
     * Get the topmost item.
     */
    std::size_t top_item(std::size_t k) const;

    /**
     * Get the bottommost item.
     */
    std::size_t bottom_item(std::size_t k) const;

    /**
     * Put an item in a stack.
     */
    void push(const Model &model, std::size_t i, std::size_t k);

    /**
     * Remove an item from its stack.
     */
    void pop(const Model &model, std::size_t i);

    /**
     * Check whether a push is feasible.
     */
    bool push_is_feasible(const Model &model, std::size_t i, std::size_t k) const;

    /**
     * Normalize the solution (reorder stacks).
     */
    void normalize();

    /**
     * Check from scratch if data was computed properly.
     */
    void check(const Model &model) const;

    /**
     * Print the solution values.
     */
    void print(std::ostream &os = std::cout) const;

    /**
     * Print the layout.
     */
    void print_layout(std::ostream &os = std::cout, const std::string &separator = "|") const;

    /**
     * Print the layout with due times.
     */
    void print_layout_d(const Model &model, std::ostream &os = std::cout, const std::string &separator = "|") const;

    /**
     * Print the layout with weights.
     */
    void print_layout_w(const Model &model, std::ostream &os = std::cout, const std::string &separator = "|") const;

    /**
     * Print the layout given a base.
     */
    void print_layout(std::function<std::string(std::size_t)> fn, const std::string &separator = "|", std::ostream &os = std::cout) const;

    /**
     * Print debug info.
     */
    void dump(std::ostream &os) const;

private:
    std::size_t _n_blocking = 0;                  // Primary objective (lower bound)
    std::size_t _n_blocked = 0;                   // Secondary objective
    std::size_t _n_used_stacks = 0;               // Number of non-empty stacks
    std::vector<std::size_t> _assignments;        // Items to stack assignments
    std::vector<std::size_t> _heights;            // Actual stack heights
    std::vector<std::size_t> _n_items_blocking;   // Counter of items blocking i
    std::vector<std::size_t> _n_items_blocked_by; // Counter of items blocked by i
};

/**
 * Print a solution.
 */
std::ostream &operator<<(std::ostream &os, const StackingSolution &sol);

#endif
