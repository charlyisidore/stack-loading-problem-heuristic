/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SCIP_EXCEPTION
#define SCIP_EXCEPTION

#include <exception>
#include <scip/scip.h>

/**
 * An exception class for SCIP.
 */
class SCIPException : public std::exception
{
public:
    /**
     * Constructor.
     */
    explicit SCIPException(SCIP_RETCODE retcode)
        : _retcode(retcode)
    {
        // Error codes from src/scip/retcode.c
        switch (retcode)
        {
            case SCIP_OKAY:
                SCIPsnprintf(_what, _max_size, "normal termination");
                break;
            case SCIP_ERROR:
                SCIPsnprintf(_what, _max_size, "unspecified error");
                break;
            case SCIP_NOMEMORY:
                SCIPsnprintf(_what, _max_size, "insufficient memory error");
                break;
            case SCIP_READERROR:
                SCIPsnprintf(_what, _max_size, "read error");
                break;
            case SCIP_WRITEERROR:
                SCIPsnprintf(_what, _max_size, "write error");
                break;
            case SCIP_NOFILE:
                SCIPsnprintf(_what, _max_size, "file not found error");
                break;
            case SCIP_FILECREATEERROR:
                SCIPsnprintf(_what, _max_size, "cannot create file");
                break;
            case SCIP_LPERROR:
                SCIPsnprintf(_what, _max_size, "error in LP solver");
                break;
            case SCIP_NOPROBLEM:
                SCIPsnprintf(_what, _max_size, "no problem exists");
                break;
            case SCIP_INVALIDCALL:
                SCIPsnprintf(_what, _max_size, "method cannot be called at this time in solution process");
                break;
            case SCIP_INVALIDDATA:
                SCIPsnprintf(_what, _max_size, "method cannot be called with this type of data");
                break;
            case SCIP_INVALIDRESULT:
                SCIPsnprintf(_what, _max_size, "method returned an invalid result code");
                break;
            case SCIP_PLUGINNOTFOUND:
                SCIPsnprintf(_what, _max_size, "a required plugin was not found");
                break;
            case SCIP_PARAMETERUNKNOWN:
                SCIPsnprintf(_what, _max_size, "the parameter with the given name was not found");
                break;
            case SCIP_PARAMETERWRONGTYPE:
                SCIPsnprintf(_what, _max_size, "the parameter is not of the expected type");
                break;
            case SCIP_PARAMETERWRONGVAL:
                SCIPsnprintf(_what, _max_size, "the value is invalid for the given parameter");
                break;
            case SCIP_KEYALREADYEXISTING:
                SCIPsnprintf(_what, _max_size, "the given key is already existing in table");
                break;
            case SCIP_MAXDEPTHLEVEL:
                SCIPsnprintf(_what, _max_size, "maximal branching depth level exceeded");
                break;
            case SCIP_BRANCHERROR:
                SCIPsnprintf(_what, _max_size, "branching could not be performed (e.g. too large values in variable domain)");
                break;
            case SCIP_NOTIMPLEMENTED:
                SCIPsnprintf(_what, _max_size, "function not implemented");
                break;
            default:
                SCIPsnprintf(_what, _max_size, "unknown SCIP retcode %d");
                break;
        }
    }

    /**
     * Destructor.
     */
    virtual ~SCIPException() {}

    /**
     * Return an explanatory string.
     */
    virtual const char *what() const noexcept
    {
        return _what;
    }

    /**
     * Get the SCIP return code.
     */
    SCIP_RETCODE retcode() const
    {
        return _retcode;
    }

private:
    static const int _max_size = 128;
    SCIP_RETCODE _retcode;
    char _what[_max_size];
};

/**
 * Macro throwing a SCIPException in case of error.
 */
#define SCIP_CALL_EXC(x)                  \
    {                                     \
        SCIP_RETCODE retcode;             \
        if ((retcode = (x)) != SCIP_OKAY) \
        {                                 \
            throw SCIPException(retcode); \
        }                                 \
    }

#endif