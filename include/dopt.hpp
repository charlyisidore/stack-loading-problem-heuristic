/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_HPP
#define DOPT_HPP

#include <argp.h>
#include <getopt.h>
#include <iostream>
#include <map>
#include <optional>
#include <set>
#include <sstream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <variant>
#include <vector>

namespace dopt
{

/**
 * Defines an option argument.
 */
class Argument
{
public:
    /**
     * Constructor.
     * 
     * Parameters:
     *   name - The argument name (e.g. "FILE").
     */
    Argument(const std::string &name)
        : _name(name), _required(true) {}

    /**
     * Copy constructor.
     */
    Argument(const Argument &) = default;

    /**
     * Move constructor.
     */
    Argument(Argument &&) = default;

    /**
     * Copy assignment operator.
     */
    Argument &operator=(const Argument &) = default;

    /**
     * Move assignment operator.
     */
    Argument &operator=(Argument &&) = default;

    /**
     * Mark the argument as required.
     */
    Argument &required()
    {
        _required = true;
        return *this;
    }

    /**
     * Mark the argument as optional.
     */
    Argument &optional()
    {
        _required = false;
        return *this;
    }

    /**
     * Return the name of the argument.
     */
    const std::string &name() const
    {
        return _name;
    }

    /**
     * Check whether the argument is required.
     */
    bool is_required() const
    {
        return _required;
    }

    /**
     * Check whether the argument is optional.
     */
    bool is_optional() const
    {
        return !_required;
    }

private:
    std::string _name;
    bool _required = true;
};

/**
 * Represents a single option name.
 */
class Name
{
public:
    /**
     * Construct from a short name.
     * 
     * Parameters:
     *   key - A short name without the "-".
     */
    Name(char key)
        : _name(key)
    {
    }

    /**
     * Construct from a long name.
     * 
     * Parameters:
     *   name - A long name without the "--".
     */
    Name(const std::string &name)
        : _name(name)
    {
    }

    /**
     * Construct from a long name.
     * 
     * Parameters:
     *   name - A long name without the "--".
     */
    Name(const char *name)
        : _name(name)
    {
    }

    /**
     * Copy constructor.
     */
    Name(const Name &) = default;

    /**
     * Move constructor.
     */
    Name(Name &&) = default;

    /**
     * Copy assignment operator.
     */
    Name &operator=(const Name &) = default;

    /**
     * Move assignment operator.
     */
    Name &operator=(Name &&) = default;

    /**
     * Check whether the name is equal to a given short name without the "-".
     */
    bool operator==(char key) const
    {
        auto k = std::get_if<char>(&_name);
        return k && *k == key;
    }

    /**
     * Check whether the name is equal to a given long name without the "--".
     */
    bool operator==(const std::string &name) const
    {
        auto n = std::get_if<std::string>(&_name);
        return n && *n == name;
    }

    /**
     * Check whether the name is short (e.g. "-n").
     */
    bool is_short() const
    {
        return std::holds_alternative<char>(_name);
    }

    /**
     * Check whether the name is long (e.g. "--name").
     */
    bool is_long() const
    {
        return std::holds_alternative<std::string>(_name);
    }

    /**
     * Cast as a short name (char) without the "-".
     */
    char get_short() const
    {
        return std::get<char>(_name);
    }

    /**
     * Cast as a long name (string) without the "--".
     */
    const std::string &get_long() const
    {
        return std::get<std::string>(_name);
    }

    /**
     * Return the name as a string (with "-" or "--").
     */
    std::string str() const
    {
        return is_short()
                   ? (std::string("-") + get_short())
                   : ("--" + get_long());
    }

private:
    std::variant<char, std::string> _name;
};

/**
 * Defines an ordering on Name objects.
 */
inline bool operator<(const Name &n1, const Name &n2)
{
    if (n1.is_short() && n2.is_short())
    {
        // Compare two short options
        return n1.get_short() < n2.get_short();
    }
    else if (n1.is_long() && n2.is_long())
    {
        // Compare two long options
        return n1.get_long() < n2.get_long();
    }
    else
    {
        // Short options come first
        return n1.is_short();
    }
}

/**
 * Stores an argument value.
 */
class Value
{
public:
    /**
     * Default constructor.
     */
    Value() = default;

    /**
     * Copy constructor.
     */
    Value(const Value &) = default;

    /**
     * Move constructor.
     */
    Value(Value &&) = default;

    /**
     * Copy assignment operator.
     */
    Value &operator=(const Value &) = default;

    /**
     * Move assignment operator.
     */
    Value &operator=(Value &&) = default;

    /**
     * Constructor.
     * 
     * Parameters:
     *   arg - A string.
     */
    Value(const std::string &arg)
        : _arg(arg) {}

    /**
     * Constructor.
     * 
     * Parameters:
     *   arg - A C-string.
     */
    Value(const char *arg)
        : _arg(arg) {}

    /**
     * Check whether the object holds a value.
     */
    bool has_value() const
    {
        return _arg.has_value();
    }

    /**
     * Return the value as a string.
     */
    const std::string &str() const
    {
        return _arg.value();
    }

    /**
     * Converts the value to given type T.
     * 
     * Parameters:
     *   default_value - A default value returned if the Value object is empty.
     */
    template <typename T>
    auto get(T default_value = T()) const
    {
        if (_arg.has_value())
        {
            std::istringstream is(_arg.value());
            is >> default_value;
        }
        return default_value;
    }

    /**
     * Check whether the object holds a value.
     */
    operator bool() const
    {
        return has_value();
    }

private:
    std::optional<std::string> _arg;
};

/**
 * Describes a program option.
 */
class Option
{
public:
    /**
     * Copy constructor.
     */
    Option(const Option &) = default;

    /**
     * Move constructor.
     */
    Option(Option &&) = default;

    /**
     * Copy assignment constructor.
     */
    Option &operator=(const Option &) = default;

    /**
     * Move assignment constructor.
     */
    Option &operator=(Option &&) = default;

    /**
     * Construct a group title.
     * 
     * Parameters:
     *   doc - A title.
     */
    Option(const std::string &doc,
           const std::set<int> &flags = std::set<int>())
        : _doc(doc), _flags(flags) {}

    /**
     * Construct an option.
     * 
     * Parameters:
     *   names - A list of option names (e.g. {'q', "quiet"}).
     *   doc - A documentation string.
     *   flags - An optional set of flags describing the option.
     */
    Option(const std::vector<Name> &names,
           const std::string &doc = std::string(),
           const std::set<int> &flags = std::set<int>())
        : _names(names), _doc(doc), _flags(flags) {}

    /**
     * Construct an option.
     * 
     * Parameters:
     *   names - A list of option names (e.g. {'q', "quiet"}).
     *   arg - An argument.
     *   doc - A documentation string.
     *   flags - An optional set of flags describing the option.
     */
    Option(const std::vector<Name> &names,
           const Argument &arg,
           const std::string &doc = std::string(),
           const std::set<int> &flags = std::set<int>())
        : _names(names), _arg(arg), _doc(doc), _flags(flags) {}

    /**
     * Construct an option.
     * 
     * Parameters:
     *   names - A list of option names (e.g. {'q', "quiet"}).
     *   doc - A documentation string.
     *   arg - An argument.
     *   flags - An optional set of flags describing the option.
     */
    Option(const std::vector<Name> &names,
           const std::string &doc,
           const Argument &arg,
           const std::set<int> &flags = std::set<int>())
        : _names(names), _arg(arg), _doc(doc), _flags(flags) {}

    /**
     * Check whether the option has an argument.
     */
    bool has_argument() const
    {
        return _arg.has_value();
    }

    /**
     * Return the option names.
     */
    const std::vector<Name> &names() const
    {
        return _names;
    }

    /**
     * Return the argument.
     */
    const Argument &argument() const
    {
        return _arg.value();
    }

    /**
     * Return the documentation string.
     */
    const std::string &doc() const
    {
        return _doc;
    }

    /**
     * Return the flags.
     */
    const std::set<int> &flags() const
    {
        return _flags;
    }

private:
    std::vector<Name> _names;
    std::optional<Argument> _arg;
    std::string _doc;
    std::set<int> _flags;
};

/**
 * Stores parsing results.
 */
class Map
{
public:
    /**
     * Default constructor.
     */
    Map() = default;

    /**
     * Copy constructor.
     */
    Map(const Map &) = default;

    /**
     * Move constructor.
     */
    Map(Map &&) = default;

    /**
     * Copy assignment constructor.
     */
    Map &operator=(const Map &) = default;

    /**
     * Move assignment constructor.
     */
    Map &operator=(Map &&) = default;

    /**
     * Return the number of positional arguments.
     */
    std::size_t size() const
    {
        return _positional.size();
    }

    /**
     * Access the value of a short option.
     */
    Value operator[](char key) const
    {
        auto i = _indices.find(key);
        return i != _indices.end() ? _named.at(i->second) : Value();
    }

    /**
     * Access the value of a long option.
     */
    Value operator[](const std::string &name) const
    {
        auto i = _indices.find(name);
        return i != _indices.end() ? _named.at(i->second) : Value();
    }

    /**
     * Access the value of a positional argument.
     */
    Value operator[](int index) const
    {
        return _positional.at(index);
    }

    /**
     * Set the value of a named option.
     */
    void assign(const std::vector<Name> &names, const Value &value)
    {
        auto it = _indices.find(names.front());
        if (it == _indices.end())
        {
            for (const auto &name : names)
            {
                _indices.insert({name, _named.size()});
            }
            _named.push_back(value);
        }
        else
        {
            _named.at(it->second) = value;
        }
    }

    /**
     * Add a positional argument.
     * 
     * Parameters:
     *   value - A value.
     */
    void push_back(const Value &value)
    {
        _positional.push_back(value);
    }

private:
    std::map<Name, std::size_t> _indices;
    std::vector<Value> _named;
    std::vector<Value> _positional;
};

/**
 * Parse command line options using Argp.
 */
class ParserArgp
{
public:
    /**
     * Constructor.
     * 
     * Parameters:
     *   args_doc - A string describing non-option arguments.
     *   doc - A descripting string about the program displayed before the options.
     *   options - A list of options.
     *   footer - An optional string to be displayed after the options.
     */
    ParserArgp(const std::string &args_doc,
               const std::string &doc,
               const std::vector<Option> &options,
               const std::string &footer = std::string())
        : _args_doc(args_doc), _doc(doc), _options(options)
    {
        if (!footer.empty())
        {
            _doc += "\v" + footer;
        }
        _build();
    }

    /**
     * Deleted copy constructor.
     */
    ParserArgp(const ParserArgp &) = delete;

    /**
     * Move constructor.
     */
    ParserArgp(ParserArgp &&) = default;

    /**
     * Deleted copy assignment operator.
     */
    ParserArgp &operator=(const ParserArgp &) = delete;

    /**
     * Move assignment operator.
     */
    ParserArgp &operator=(ParserArgp &&) = default;

    /**
     * Parse a given command line.
     */
    Map operator()(int argc,
                   char *argv[],
                   const std::set<int> &flags = std::set<int>()) const
    {
        return _parse(argc, argv, flags);
    }

private:
    typedef std::tuple<const ParserArgp &, Map &> _ArgpInput;

    /**
     * Build an array of argp_option.
     */
    void _build()
    {
        int index = 0x100;

        for (const auto &option : _options)
        {
            struct ::argp_option opt = {};

            if (option.has_argument())
            {
                const auto &arg = option.argument();
                opt.arg = arg.name().c_str();
                if (arg.is_optional())
                {
                    opt.flags |= OPTION_ARG_OPTIONAL;
                }
            }

            for (auto flag : option.flags())
            {
                opt.flags |= flag;
            }

            const auto &names = option.names();
            if (!names.empty())
            {
                // Named option
                for (auto n = std::begin(names); n != std::end(names); ++n)
                {
                    if (n->is_short())
                    {
                        opt.key = n->get_short();
                        opt.name = 0;
                    }
                    else
                    {
                        opt.key = index++;
                        opt.name = n->get_long().c_str();
                    }

                    if (n == std::begin(names))
                    {
                        opt.doc = option.doc().c_str();
                    }
                    else
                    {
                        opt.flags |= OPTION_ALIAS;
                        opt.arg = 0;
                        opt.doc = 0;
                    }

                    _argp_keys.insert({opt.key, option});
                    _argp_opts.push_back(opt);
                }
            }
            else
            {
                // Option group title
                opt.doc = option.doc().c_str();
                _argp_opts.push_back(opt);
            }
        }
        _argp_opts.push_back({});
    }

    /**
     * Parse a given command line.
     */
    Map _parse(int argc,
               char *argv[],
               const std::set<int> &flags = std::set<int>()) const
    {
        Map map;
        _ArgpInput input(*this, map);
        int argp_flags = 0;

        struct ::argp argp = {
            _argp_opts.data(),
            _parse_opt,
            _args_doc.c_str(),
            _doc.c_str(),
            nullptr,
            nullptr,
            nullptr};

        for (auto flag : flags)
        {
            argp_flags |= flag;
        }

        argp_parse(&argp, argc, argv, argp_flags, 0, &input);
        return map;
    }

    /**
     * A function called by Argp to parse a single option.
     * 
     * Parameters:
     *   key - Identifier of the option.
     *   arg - Argument of the option, or nullptr if it has none.
     *   state - A pointer to a struct argp_state.
     */
    static error_t _parse_opt(int key, char *arg, struct ::argp_state *state)
    {
        auto [parser, map] = *static_cast<_ArgpInput *>(state->input);

        switch (key)
        {
            case ARGP_KEY_INIT:    // Before parsing is done
            case ARGP_KEY_NO_ARGS: // No positional argument found
            case ARGP_KEY_END:     // No more arguments
            case ARGP_KEY_SUCCESS: // Parsing has been completed
            case ARGP_KEY_ERROR:   // An error has occurred
            case ARGP_KEY_FINI:    // Called after succes/error
            case ARGP_KEY_ARGS:    //
                break;

            case ARGP_KEY_ARG:
                // A positional argument has been found
                map.push_back({arg});
                break;

            default:
                // Named argument or Argp special key
                auto it = parser._argp_keys.find(key);
                if (it == parser._argp_keys.end())
                {
                    // Argp special key?
                    return ARGP_ERR_UNKNOWN;
                }
                map.assign(it->second.names(), arg ? arg : std::string());
                return 0;
        }

        return 0;
    }

    std::string _args_doc;
    std::string _doc;
    std::vector<Option> _options;
    std::unordered_map<int, const Option &> _argp_keys;
    std::vector<struct ::argp_option> _argp_opts;
};

/**
 * Parse command line options using getopt.
 */
class ParserGetopt
{
public:
    /**
     * Constructor.
     * 
     * Parameters:
     *   options - A list of options.
     */
    ParserGetopt(const std::vector<Option> &options)
        : _options(options)
    {
        _build();
    }

    /**
     * Deleted copy constructor.
     */
    ParserGetopt(const ParserGetopt &) = delete;

    /**
     * Move constructor.
     */
    ParserGetopt(ParserGetopt &&) = default;

    /**
     * Deleted copy assignment operator.
     */
    ParserGetopt &operator=(const ParserGetopt &) = delete;

    /**
     * Move assignment operator.
     */
    ParserGetopt &operator=(ParserGetopt &&) = default;

    /**
     * Parse a given command line.
     */
    Map operator()(int argc, char *argv[]) const
    {
        return _parse(argc, argv);
    }

private:
    /**
     * Build an array of getopt options.
     */
    void _build()
    {
        int index = 0x100;

        for (const auto &option : _options)
        {
            const auto &names = option.names();
            for (auto n = std::begin(names); n != std::end(names); ++n)
            {
                if (n->is_short())
                {
                    char key = n->get_short();
                    _shortopts += key;
                    if (option.has_argument())
                    {
                        if (option.argument().is_required())
                        {
                            _shortopts += ":";
                        }
                        else // is_optional
                        {
                            _shortopts += "::";
                        }
                    }
                    _getopt_keys.insert({key, option});
                }
                else // is_long
                {
                    struct ::option longopt = {};
                    longopt.name = n->get_long().c_str();
                    if (option.has_argument())
                    {
                        if (option.argument().is_required())
                        {
                            longopt.has_arg = required_argument;
                        }
                        else // is_optional
                        {
                            longopt.has_arg = optional_argument;
                        }
                    }
                    else
                    {
                        longopt.has_arg = no_argument;
                    }
                    longopt.val = index++;
                    _getopt_keys.insert({longopt.val, option});
                    _longopts.push_back(longopt);
                }
            }
        }
        _longopts.push_back({});
    }

    /**
     * Parse a given command line.
     */
    Map _parse(int argc, char *argv[]) const
    {
        Map map;
        int index = 0;

        for (int key = 0; key != -1;)
        {
            key = getopt_long(argc,
                              argv,
                              _shortopts.c_str(),
                              _longopts.data(),
                              &index);

            switch (key)
            {
                case -1: // End of options
                case 0:  // Option setting a flag
                    break;

                case '?':
                    // getopt already has printed an error message
                    // optopt != 0 => requires an argument
                    // optopt == 0 => unrecognized option
                    std::exit(1);

                default:
                    auto it = _getopt_keys.find(key);
                    if (it == _getopt_keys.end())
                    {
                        std::cerr << "unknown option character `\\x" << std::hex << key << "'." << std::endl;
                        std::exit(1);
                    }
                    map.assign(it->second.names(), optarg ? optarg : std::string());
                    break;
            }
        }

        // Positional arguments
        while (optind < argc)
        {
            map.push_back({argv[optind++]});
        }
        return map;
    }

    std::vector<Option> _options;
    std::unordered_map<int, const Option &> _getopt_keys;
    std::string _shortopts;
    std::vector<struct ::option> _longopts;
};

} // namespace dopt

#endif
