/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STACKING_HEURISTIC_HPP
#define STACKING_HEURISTIC_HPP

#include "stacking_model.hpp"
#include "stacking_solution.hpp"
#include <functional>
#include <random>
#include <vector>

// Degree sort rule
class _StackingHeuristic_DegreeSort
{
public:
    typedef StackingModel Model;

    _StackingHeuristic_DegreeSort(const Model &model)
    {
        s_deg.resize(model.n_items(), 0);
        r_deg.resize(model.n_items(), 0);

        for (std::size_t i = 0; i < model.n_items(); ++i)
        {
            for (std::size_t j = 0; j < i; ++j)
            {
                if (!model.s(i, j))
                {
                    ++s_deg[i];
                    ++s_deg[j];
                }
                else if (model.r(i, j))
                {
                    ++r_deg[i];
                    ++r_deg[j];
                }
            }
        }
    }

    bool operator()(std::size_t i, std::size_t j) const
    {
        return s_deg[i] > s_deg[j] ||
               (s_deg[i] == s_deg[j] &&
                (r_deg[i] > r_deg[j] || (r_deg[i] == r_deg[j] && i < j)));
    }

private:
    std::vector<std::size_t> s_deg, r_deg;
};

// LIFO sort rule
class _StackingHeuristic_LIFOSort
{
public:
    bool operator()(std::size_t i, std::size_t j) const
    {
        return i < j;
    }
};

// FIFO sort rule
class _StackingHeuristic_FIFOSort
{
public:
    bool operator()(std::size_t i, std::size_t j) const
    {
        return i > j;
    }
};

// Geo selection rule
class _StackingHeuristic_GeoSelect
{
public:
    typedef StackingModel Model;
    typedef StackingSolution Solution;

    _StackingHeuristic_GeoSelect(std::mt19937 &rng,
                                 double randomness = 0.1,
                                 bool extended = true,
                                 bool allow_violations = false,
                                 bool debug = false)
        : _rng(rng),
          _randomness(randomness),
          _extended(extended),
          _allow_violations(allow_violations),
          _debug(debug)
    {
    }
    _StackingHeuristic_GeoSelect(const _StackingHeuristic_GeoSelect &) = default;

    std::size_t operator()(const Model &model, const Solution &sol,
                           std::size_t i) const;

private:
    std::mt19937 &_rng;
    double _randomness = 0.1;
    bool _extended = true;
    bool _allow_violations = false;
    bool _debug = false;
};

/**
 * Heuristics for the Stacking Problem.
 */
class StackingHeuristic
{
public:
    /**
     * Model type definition.
     */
    typedef StackingModel Model;

    /**
     * Solution type definition.
     */
    typedef StackingSolution Solution;

    /**
     * Sort rule type definition.
     */
    typedef std::function<bool(std::size_t i, std::size_t j)> SortRule;

    /**
     * Selection rule type definition.
     */
    typedef std::function<std::size_t(const Model &model, const Solution &sol, std::size_t i)> SelectRule;

    /**
     * Degree sort rule.
     */
    typedef _StackingHeuristic_DegreeSort DegreeSort;

    /**
     * LIFO sort rule.
     */
    typedef _StackingHeuristic_LIFOSort LIFOSort;

    /**
     * FIFO sort rule.
     */
    typedef _StackingHeuristic_FIFOSort FIFOSort;

    /**
     * Geo selection rule.
     */
    typedef _StackingHeuristic_GeoSelect GeoSelect;

    /**
     * Constructor.
     */
    StackingHeuristic(std::mt19937 &rng);

    /**
     * Copy constructor.
     */
    StackingHeuristic(const StackingHeuristic &) = default;

    /**
     * Copy assignment operator.
     */
    StackingHeuristic &operator=(const StackingHeuristic &) = default;

    /**
     * Get the model.
     */
    const Model &model() const;

    /**
     * Get the solution.
     */
    const Solution &solution() const;

    /**
     * Get the number of repairs.
     */
    std::size_t n_repairs() const;

    /**
     * Get the number of 1-opt moves.
     */
    std::size_t n_1_opt_moves() const;

    /**
     * Get the number of 2-opt moves.
     */
    std::size_t n_2_opt_moves() const;

    /**
     * Set the model.
     */
    void set_model(const Model &model);

    /**
     * Set the sort rule.
     */
    template <typename SortF>
    void set_sort_rule(SortF fn)
    {
        _sort = fn;
    }

    /**
     * Set the select rule.
     */
    template <typename SelectF>
    void set_select_rule(SelectF fn)
    {
        _select = fn;
    }

    /**
     * Set the repair mode.
     */
    void set_repair(bool repair);

    /**
     * Allow stacking violations.
     */
    void set_allow_violations(bool allow_violations);

    /**
     * Set the debug mode.
     */
    void set_debug(bool debug);

    /**
     * Construct a solution.
     */
    void construct();

    /**
     * Attempt to move items for inserting item i.
     */
    bool repair(std::size_t i);

    /**
     * Improve the solution with a local search.
     */
    bool local_search(unsigned int depth = 2, bool extended = true, bool only_exchange = false);

    /**
     * Apply 1-opt local search.
     */
    bool local_search_1_opt(bool extended = true);

    /**
     * Apply 2-opt local search.
     */
    bool local_search_2_opt(bool extended = true, bool only_exchange = false);

    /**
     * Normalize the solution.
     */
    void normalize_solution();

private:
    Model _model;
    Solution _solution;
    SortRule _sort;
    SelectRule _select;
    bool _repair = true;
    bool _allow_violations = false;
    std::mt19937 &_rng;
    std::size_t _n_repairs = 0;
    std::size_t _n_1_opt_moves = 0;
    std::size_t _n_2_opt_moves = 0;
    bool _debug = false;
};

/**
 * Count the number of blocking items added after a push.
 */
std::size_t n_blocking_push(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k);

/**
 * Count the number of blocking items added after a push.
 */
std::size_t n_blocked_push(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k);

/**
 * Count the number of blocking items removed after a 1-opt.
 */
std::size_t n_blocking_1_opt(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k);

/**
 * Count the number of blocking items removed after a 1-opt.
 */
std::size_t n_blocked_1_opt(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k);

/**
 * Check whether a 2-opt move is feasible.
 */
bool is_feasible_2_opt(const StackingModel &model, const StackingSolution &sol, std::size_t i1, std::size_t k1, std::size_t i2, std::size_t k2);

#endif
