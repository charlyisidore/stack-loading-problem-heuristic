/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMMAND_GUROBI_HPP
#define COMMAND_GUROBI_HPP

#include "command.hpp"
#include <stdexcept>

#if USE_GUROBI

#include "stacking_gurobi.hpp"
#include "stacking_model.hpp"
#include "stacking_solution.hpp"
#include <memory>
#include <string>

/**
 * Run the "gurobi" command line.
 */
class CommandGUROBI : public Command
{
public:
    /**
     * Read command line options.
     */
    virtual void read_options(const dopt::Map &args) override;

    /**
     * Print command line options.
     */
    virtual void print_options(std::ostream &os) const override;

    /**
     * Initialize the solver.
     */
    virtual void initialize(const StackingModel &m) override;

    /**
     * Run the solver.
     */
    virtual void solve() override;

    /**
     * Build the solution.
     */
    virtual StackingSolution finalize() override;

    /**
     * Set the output data.
     */
    virtual void output(nlohmann::json &data, const StackingSolution &solution) const override;

private:
    unsigned int time_limit = 0;
    unsigned int memory_limit = 0;
    std::string log_filename;
    std::string model_filename;
    std::string sol_filename;
    bool allow_violations = false;
    bool debug = false;
    bool verbose = true;
    StackingModel model;
    std::unique_ptr<StackingGUROBI> solver;
};

#else

class CommandGUROBI : public Command
{
public:
    CommandGUROBI()
    {
        throw std::runtime_error("GUROBI not supported");
    }
};

#endif

#endif