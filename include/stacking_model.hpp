/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STACKING_MODEL_HPP
#define STACKING_MODEL_HPP

#include <iostream>
#include <vector>

/**
 * Model description for the Stacking Problem.
 */
class StackingModel
{
public:
    /**
     * Default constructor.
     */
    StackingModel() = default;

    /**
     * Copy constructor.
     */
    StackingModel(const StackingModel &) = default;

    /**
     * Move constructor.
     */
    StackingModel(StackingModel &&) = default;

    /**
     * Copy assignment operator.
     */
    StackingModel &operator=(const StackingModel &) = default;

    /**
     * Move assignment operator.
     */
    StackingModel &operator=(StackingModel &&) = default;

    /**
     * Check whether stacking constraints are arbitrary.
     */
    bool is_arbitrary() const;

    /**
     * Get the number of items.
     */
    std::size_t n_items() const;

    /**
     * Get the number of stacks.
     */
    std::size_t n_stacks() const;

    /**
     * Get the capacity of stacks.
     */
    std::size_t capacity() const;

    /**
     * Get the number of initial items.
     */
    std::size_t n_initial_items() const;

    /**
     * Get the initial position of an item.
     */
    std::size_t initial_position(std::size_t i) const;

    /**
     * Get departure time of an item.
     */
    double departure(std::size_t i) const;

    /**
     * Get weight of an item.
     */
    double weight(std::size_t i) const;

    /**
     * Get all departure times.
     */
    const std::vector<double> &departures() const;

    /**
     * Get all weights.
     */
    const std::vector<double> &weights() const;

    /**
     * Return true if item i blocks item j if put together.
     */
    bool r(std::size_t i, std::size_t j) const;

    /**
     * Return true if item i can be put with item j together.
     */
    bool s(std::size_t i, std::size_t j) const;

    /**
     * Return the list of items that conflict with item i, given r_ij.
     */
    const std::vector<std::size_t> &r_list(std::size_t i) const;

    /**
     * Return the list of items that conflict with item i, given s_ij.
     */
    const std::vector<std::size_t> &s_list(std::size_t i) const;

    /**
     * Read an instance.
     */
    void read(std::istream &is);

    /**
     * Write an instance.
     */
    void write(std::ostream &os) const;

private:
    /**
     * Compute matrices.
     */
    void _compute_matrices();

    /**
     * Compute conflict lists.
     */
    void _compute_conflict_lists();

    std::size_t _n_stacks;                         // Number of stacks.
    std::size_t _capacity;                         // Capacity per stack.
    std::vector<double> _d;                        // Due time of items, if available.
    std::vector<double> _w;                        // Weight of items, if available.
    std::vector<std::vector<bool>> _r;             // Soft stacking constraints.
    std::vector<std::vector<bool>> _s;             // Hard stacking constraints.
    std::vector<std::vector<std::size_t>> _r_list; // List of items in conflict with given item (given r_ij).
    std::vector<std::vector<std::size_t>> _s_list; // List of items in conflict with given item (given s_ij).
    std::vector<std::size_t> _kfix;                // Position of initial items.
};

/**
 * Print a model.
 */
std::ostream &operator<<(std::ostream &os, const StackingModel &model);

#endif
