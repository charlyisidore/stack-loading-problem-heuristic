/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STACKING_CPLEX_HPP
#define STACKING_CPLEX_HPP

#include "stacking_model.hpp"
#include "stacking_solution.hpp"
#include <ilcplex/ilocplex.h>
#include <iostream>

/**
 * Solve the Stacking Problem using CPLEX.
 */
class StackingCPLEX
{
public:
    /**
     * Model type definition.
     */
    typedef StackingModel Model;

    /**
     * Solution type definition.
     */
    typedef StackingSolution Solution;

    /**
     * Constructor.
     */
    StackingCPLEX();

    /**
     * Get the status of the solver.
     *
     * Status:
     * - IloAlgorithm::Unknown
     * - IloAlgorithm::Feasible
     * - IloAlgorithm::Optimal
     * - IloAlgorithm::Infeasible
     * - IloAlgorithm::Unbounded
     * - IloAlgorithm::InfeasibleOrUnbounded
     * - IloAlgorithm::Error
     */
    IloAlgorithm::Status status() const;

    /**
     * Check whether the solution is optimal.
     */
    bool is_optimal() const;

    /**
     * Check whether the solution is feasible.
     */
    bool is_feasible() const;

    /**
     * Get the solution.
     */
    const Solution &solution() const;

    /**
     * Get the CPLEX parameter set.
     */
    IloCplex::ParameterSet parameter_set();

    /**
     * Set the CPLEX parameter set.
     */
    void set_parameter_set(IloCplex::ParameterSet set);

    /**
     * Set the model.
     */
    void set_model(const Model &model);

    /**
     * Determine whether violations are allowed or not.
     */
    void set_allow_violations(bool allow);

    /**
     * Determine whether the model includes valid inequalities or not.
     */
    void set_valid_inequality(bool valid_inequality);

    /**
     * Set the quiet mode.
     */
    void set_quiet();

    /**
     * Set the output stream.
     */
    void set_out_stream(std::ostream &os);

    /**
     * Set the warning stream.
     */
    void set_warning_stream(std::ostream &os);

    /**
     * Set the error stream.
     */
    void set_error_stream(std::ostream &os);

    /**
     * Set the debug mode.
     */
    void set_debug(bool debug);

    /**
     * Build the MIP model.
     */
    void build_model();

    /**
     * Run the solver.
     */
    void run();

    /**
     * Build the solution.
     */
    void build_solution();

    /**
     * Get the number of valid inequalities.
     */
    std::size_t n_valid_inequalities() const;

    /**
     * Export the model.
     */
    void export_model(const char *filename) const;

    /**
     * Export the solutions.
     */
    void export_solutions(const char *filename) const;

private:
    IloEnv _env;
    IloModel _model;
    IloObjective _objective;
    IloArray<IloNumVarArray> _x;
    IloNumVarArray _z;
    IloNumVarArray _v;
    IloCplex _solver;
    Model _stacking_model;
    Solution _solution;
    std::size_t _n_fixed_blocking_items = 0;
    std::size_t _n_fixed_violating_items = 0;
    bool _allow_violations = false;
    bool _valid_inequality = true;
    std::size_t _n_valid_inequalities = 0;
    bool _debug = false;
};

#endif
