/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COMMAND_HEURISTIC_HPP
#define COMMAND_HEURISTIC_HPP

#include "command.hpp"
#include <stdexcept>

#include "stacking_heuristic.hpp"
#include "stacking_model.hpp"
#include "stacking_solution.hpp"
#include <fstream>
#include <iostream>
#include <random>
#include <string>

/**
 * Run the "heuristic" command line.
 */
class CommandHeuristic : public Command
{
public:
    /**
     * Constructor.
     */
    CommandHeuristic();

    /**
     * Read command line options.
     */
    virtual void read_options(const dopt::Map &args) override;

    /**
     * Print command line options.
     */
    virtual void print_options(std::ostream &os) const override;

    /**
     * Initialize the solver.
     */
    virtual void initialize(const StackingModel &m) override;

    /**
     * Run the solver.
     */
    virtual void solve() override;

    /**
     * Build the solution.
     */
    virtual StackingSolution finalize() override;

    /**
     * Set the output data.
     */
    virtual void output(nlohmann::json &data, const StackingSolution &solution) const override;

private:
    std::size_t n_iterations = 1;
    std::string sort_name = "degree";
    double randomness = 0.1;
    bool extended_construct = false;
    bool repair = true;
    unsigned int local_search_depth = 2;
    bool only_exchange = false;
    bool extended_local_search = true;
    bool allow_violations = false;
    unsigned int seed;
    bool debug = false;
    bool verbose = true;
    StackingModel model;
    std::mt19937 rng;
    StackingHeuristic solver;
    StackingSolution best_solution;
    std::size_t n_blocking_items_construct;
    std::size_t n_blocked_items_construct;
    double construct_time;
    double local_search_time;
};

#endif