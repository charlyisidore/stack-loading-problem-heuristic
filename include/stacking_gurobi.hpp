/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STACKING_GUROBI_HPP
#define STACKING_GUROBI_HPP

#include "stacking_model.hpp"
#include "stacking_solution.hpp"
#include <gurobi_c++.h>
#include <iostream>
#include <memory>
#include <vector>

/**
 * Solve the Stacking Problem using GUROBI.
 */
class StackingGUROBI
{
public:
    /**
     * Model type definition.
     */
    typedef StackingModel Model;

    /**
     * Solution type definition.
     */
    typedef StackingSolution Solution;

    /**
     * Constructor.
     */
    StackingGUROBI();

    /**
     * Get the status of the solver.
     *
     * Status:
     * - GRB_LOADED
     * - GRB_OPTIMAL
     * - GRB_INFEASIBLE
     * - GRB_INF_OR_UNBD
     * - GRB_UNBOUNDED
     * - GRB_CUTOFF
     * - GRB_ITERATION_LIMIT
     * - GRB_NODE_LIMIT
     * - GRB_TIME_LIMIT
     * - GRB_SOLUTION_LIMIT
     * - GRB_INTERRUPTED
     * - GRB_NUMERIC
     * - GRB_SUBOPTIMAL
     * - GRB_INPROGRESS
     * - GRB_USER_OBJ_LIMIT
     */
    int status() const;

    /**
     * Check whether the solution is optimal.
     */
    bool is_optimal() const;

    /**
     * Check whether the solution is feasible.
     */
    bool is_feasible() const;

    /**
     * Get the solution.
     */
    const Solution &solution() const;

    /**
     * Set a GUROBI double parameter.
     */
    void set_parameter(GRB_DoubleParam param, double value);

    /**
     * Set a GUROBI int parameter.
     */
    void set_parameter(GRB_IntParam param, int value);

    /**
     * Set a GUROBI string parameter.
     */
    void set_parameter(GRB_StringParam param, const std::string &value);

    /**
     * Set the model.
     */
    void set_model(const Model &model);

    /**
     * Determine whether violations are allowed or not.
     */
    void set_allow_violations(bool allow);

    /**
     * Set the debug mode.
     */
    void set_debug(bool debug);

    /**
     * Build the MIP model.
     */
    void build_model();

    /**
     * Run the solver.
     */
    void run();

    /**
     * Build the solution.
     */
    void build_solution();

    /**
     * Export the model.
     */
    void export_model(const char *filename);

    /**
     * Export the solutions.
     */
    void export_solutions(const char *filename) const;

private:
    GRBEnv _env;
    std::unique_ptr<GRBModel> _model;
    std::vector<std::vector<GRBVar>> _x;
    std::vector<GRBVar> _z;
    std::vector<GRBVar> _v;
    Model _stacking_model;
    Solution _solution;
    bool _allow_violations = false;
    bool _debug = false;
};

#endif
