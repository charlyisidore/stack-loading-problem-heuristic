/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef STACKING_SCIP_HPP
#define STACKING_SCIP_HPP

#include "stacking_model.hpp"
#include "stacking_solution.hpp"
#include <scip/scip.h>
#include <vector>

/**
 * Solve the Stacking Problem using SCIP.
 */
class StackingSCIP
{
public:
    /**
     * Model type definition.
     */
    typedef StackingModel Model;

    /**
     * Solution type definition.
     */
    typedef StackingSolution Solution;

    /**
     * Constructor.
     */
    StackingSCIP();

    /**
     * Destructor.
     */
    ~StackingSCIP();

    /**
     * Get the status of the solver.
     */
    SCIP_STATUS status() const;

    /**
     * Check whether the solution is optimal.
     */
    bool is_optimal() const;

    /**
     * Check whether the solution is feasible.
     */
    bool is_feasible() const;

    /**
     * Get the solution.
     */
    const Solution &solution() const;

    /**
     * Set a SCIP bool parameter.
     */
    void set_bool_param(const char *name, bool value);

    /**
     * Set a SCIP int parameter.
     */
    void set_int_param(const char *name, int value);

    /**
     * Set a SCIP long int parameter.
     */
    void set_long_int_param(const char *name, long long value);

    /**
     * Set a SCIP real parameter.
     */
    void set_real_param(const char *name, double value);

    /**
     * Set the log filename.
     */
    void set_log_file(const char *name);

    /**
     * Set the model.
     */
    void set_model(const Model &model);

    /**
     * Determine whether violations are allowed or not.
     */
    void set_allow_violations(bool allow);

    /**
     * Set the quiet mode.
     */
    void set_quiet();

    /**
     * Set the debug mode.
     */
    void set_debug(bool debug);

    /**
     * Build the MIP model.
     */
    void build_model();

    /**
     * Run the solver.
     */
    void run();

    /**
     * Build the solution.
     */
    void build_solution();

    /**
     * Free memory.
     */
    void release();

    /**
     * Export the model.
     */
    void export_model(const char *filename) const;

    /**
     * Export the solutions.
     */
    void export_solutions(const char *filename) const;

private:
    SCIP *_scip;
    std::vector<SCIP_VAR *> _z;
    std::vector<SCIP_VAR *> _v;
    std::vector<std::vector<SCIP_VAR *>> _x;
    Model _model;
    Solution _solution;
    bool _allow_violations = false;
    bool _debug = false;
};

#endif
