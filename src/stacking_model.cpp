/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stacking_model.hpp"
#include <algorithm>
#include <nlohmann/json.hpp>
#include <stdexcept>

bool StackingModel::is_arbitrary() const
{
    return _d.empty() || _w.empty();
}

std::size_t StackingModel::n_items() const
{
    return _r.size();
}

std::size_t StackingModel::n_stacks() const
{
    return _n_stacks;
}

std::size_t StackingModel::capacity() const
{
    return _capacity;
}

std::size_t StackingModel::n_initial_items() const
{
    return _kfix.size();
}

std::size_t StackingModel::initial_position(std::size_t i) const
{
    return _kfix[i];
}

double StackingModel::departure(std::size_t i) const
{
    return _d[i];
}

double StackingModel::weight(std::size_t i) const
{
    return _w[i];
}

const std::vector<double> &StackingModel::departures() const
{
    return _d;
}

const std::vector<double> &StackingModel::weights() const
{
    return _w;
}

bool StackingModel::r(std::size_t i, std::size_t j) const
{
    return _r[i][j];
}

bool StackingModel::s(std::size_t i, std::size_t j) const
{
    return _s[i][j];
}

const std::vector<std::size_t> &StackingModel::r_list(std::size_t i) const
{
    return _r_list[i];
}

const std::vector<std::size_t> &StackingModel::s_list(std::size_t i) const
{
    return _s_list[i];
}

void StackingModel::read(std::istream &is)
{
    std::size_t n;
    nlohmann::json data;

    is >> data;

    auto m_n_items = data.find("n_items");
    auto m_n_stacks = data.find("n_stacks");
    auto m_capacity = data.find("capacity");
    auto m_departures = data.find("departures");
    auto m_blocking_matrix = data.find("blocking_matrix");
    auto m_weights = data.find("weights");
    auto m_conflict_matrix = data.find("conflict_matrix");
    auto m_initial_positions = data.find("initial_positions");

    if (m_n_items == data.end())
    {
        throw std::runtime_error("StackingModel::read: missing value: n_items");
    }

    if (m_n_stacks == data.end())
    {
        throw std::runtime_error("StackingModel::read: missing value: n_stacks");
    }

    if (m_capacity == data.end())
    {
        throw std::runtime_error("StackingModel::read: missing value: capacity");
    }

    if (m_departures == data.end() && m_blocking_matrix == data.end())
    {
        throw std::runtime_error("StackingModel::read: missing value: departures or blocking_matrix");
    }

    n = m_n_items->get<std::size_t>();
    _n_stacks = m_n_stacks->get<std::size_t>();
    _capacity = m_capacity->get<std::size_t>();

    // Quick bound checking

    if (m_departures != data.end() && m_departures->size() < n)
    {
        throw std::runtime_error("StackingModel::read: departures missing values");
    }

    if (m_weights != data.end() && m_weights->size() < n)
    {
        throw std::runtime_error("StackingModel::read: weights missing values");
    }

    if (m_blocking_matrix != data.end() && m_blocking_matrix->size() < n)
    {
        throw std::runtime_error("StackingModel::read: blocking_matrix missing values");
    }

    if (m_conflict_matrix != data.end() && m_conflict_matrix->size() < n)
    {
        throw std::runtime_error("StackingModel::read: conflict_matrix missing values");
    }

    if (m_initial_positions != data.end() && m_initial_positions->size() > n)
    {
        throw std::runtime_error("StackingModel::read: initial_positions too many values");
    }

    _d.clear();
    _w.clear();
    _r.clear();
    _s.clear();
    _r_list.clear();
    _s_list.clear();
    _kfix.clear();

    _r.resize(n, std::vector<bool>(n));
    _s.resize(n, std::vector<bool>(n));

    // Due times
    if (m_departures != data.end())
    {
        _d.resize(n);
        for (std::size_t i = 0; i < n; ++i)
        {
            _d[i] = (*m_departures)[i].get<double>();
        }
    }
    // r_ij matrix
    else
    {
        for (std::size_t i = 0; i < n; ++i)
        {
            for (std::size_t j = 0; j < n; ++j)
            {
                _r[i][j] = ((*m_blocking_matrix)[i][j].get<int>() == 0) ? false : true;
            }
        }
    }

    // Weights
    if (m_weights != data.end())
    {
        _w.resize(n);
        for (std::size_t i = 0; i < n; ++i)
        {
            _w[i] = (*m_weights)[i].get<double>();
        }
    }
    // s_ij matrix
    else if (m_conflict_matrix != data.end())
    {
        for (std::size_t i = 0; i < n; ++i)
        {
            for (std::size_t j = 0; j < n; ++j)
            {
                _s[i][j] = ((*m_conflict_matrix)[i][j].get<int>() == 0) ? true : false;
            }
        }
    }
    // No stacking constraints
    else
    {
        _w.resize(n, 0);
    }

    // Initial positions
    if (m_initial_positions != data.end())
    {
        std::size_t nfix = m_initial_positions->size();
        _kfix.resize(nfix);
        for (std::size_t i = 0; i < nfix; ++i)
        {
            _kfix[i] = (*m_initial_positions)[i].get<std::size_t>() - 1;
        }
    }

    _compute_matrices();
    _compute_conflict_lists();
}

void StackingModel::write(std::ostream &os) const
{
    nlohmann::json data = nlohmann::json::object();

    data["n_items"] = n_items();
    data["n_stacks"] = n_stacks();
    data["capacity"] = capacity();

    // r_ij matrix
    if (_d.empty())
    {
        data["blocking_matrix"] = nlohmann::json::array();
        for (std::size_t i = 0; i < _r.size(); ++i)
        {
            nlohmann::json row = nlohmann::json::array();
            for (std::size_t j = 0; j < _r[i].size(); ++j)
            {
                row.push_back(_r[i][j] ? 1 : 0);
            }
            data["blocking_matrix"].push_back(row);
        }
    }
    // Due times
    else
    {
        data["departures"] = _d;
    }

    // s_ij matrix
    if (_w.empty())
    {
        data["conflict_matrix"] = nlohmann::json::array();
        for (std::size_t i = 0; i < _s.size(); ++i)
        {
            nlohmann::json row = nlohmann::json::array();
            for (std::size_t j = 0; j < _s[i].size(); ++j)
            {
                row.push_back(_s[i][j] ? 0 : 1);
            }
            data["conflict_matrix"].push_back(row);
        }
    }
    // Weights
    else
    {
        // No need to specify weights if they are all zero
        if (std::any_of(std::begin(_w), std::end(_w), [](double w) { return w != 0; }))
        {
            data["weights"] = _w;
        }
    }

    // Initial items
    if (!_kfix.empty())
    {
        data["initial_positions"] = nlohmann::json::array();
        for (auto k : _kfix)
        {
            data["initial_positions"].push_back(k + 1);
        }
    }

    os << data;
}

void StackingModel::_compute_matrices()
{
    if (_d.empty() && _w.empty())
    {
        return;
    }

    for (std::size_t i = 0; i < n_items(); ++i)
    {
        for (std::size_t j = 0; j < n_items(); ++j)
        {
            if (!_d.empty())
            {
                _r[i][j] = (_d[i] > _d[j]);
            }

            if (!_w.empty())
            {
                _s[i][j] = (_w[i] <= _w[j]);
            }
        }
    }
}

void StackingModel::_compute_conflict_lists()
{
    _r_list.resize(n_items());
    _s_list.resize(n_items());

    for (std::size_t i = 0; i < n_items(); ++i)
    {
        for (std::size_t j = 0; j < i; ++j)
        {
            if (_r[i][j])
            {
                _r_list[i].push_back(j);
                _r_list[j].push_back(i);
            }

            if (!_s[i][j])
            {
                _s_list[i].push_back(j);
                _s_list[j].push_back(i);
            }
        }
    }
}

std::ostream &operator<<(std::ostream &os, const StackingModel &model)
{
    model.write(os);
    return os;
}