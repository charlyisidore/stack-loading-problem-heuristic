/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stacking_gurobi.hpp"
#include <sstream>
#include <stdexcept>

StackingGUROBI::StackingGUROBI()
{
    _env.start();
}

int StackingGUROBI::status() const
{
    return _model->get(GRB_IntAttr_Status);
}

bool StackingGUROBI::is_optimal() const
{
    return _model->get(GRB_IntAttr_Status) == GRB_OPTIMAL;
}

bool StackingGUROBI::is_feasible() const
{
    return _model->get(GRB_IntAttr_SolCount) > 0;
}

const StackingGUROBI::Solution &StackingGUROBI::solution() const
{
    return _solution;
}

void StackingGUROBI::set_parameter(GRB_DoubleParam param, double value)
{
    _env.set(param, value);
}

void StackingGUROBI::set_parameter(GRB_IntParam param, int value)
{
    _env.set(param, value);
}

void StackingGUROBI::set_parameter(GRB_StringParam param, const std::string &value)
{
    _env.set(param, value);
}

void StackingGUROBI::set_model(const Model &model)
{
    _stacking_model = model;
}

void StackingGUROBI::set_allow_violations(bool allow)
{
    _allow_violations = allow;
}

void StackingGUROBI::set_debug(bool debug)
{
    _debug = debug;
}

void StackingGUROBI::build_model()
{
    const Model &m = _stacking_model;

    _model = std::make_unique<GRBModel>(_env);

    // Blocking items variables
    _z.reserve(m.n_items());
    for (std::size_t i = 0; i < m.n_items(); ++i)
    {
        GRBVar var;
        std::ostringstream oss;

        oss << "z{" << i + 1 << "}";
        var = _model->addVar(0, 1, 1, GRB_BINARY, oss.str());
        _z.push_back(var);
    }

    if (_allow_violations)
    {
        _v.reserve(m.n_items());
        for (std::size_t i = 0; i < m.n_items(); ++i)
        {
            GRBVar var;
            std::ostringstream oss;

            oss << "v{" << i + 1 << "}";
            var = _model->addVar(0, 1, m.n_items(), GRB_BINARY, oss.str());
            _v.push_back(var);
        }
    }

    // Assignment variables
    _x.resize(m.n_items());
    for (std::size_t i = 0; i < m.n_items(); ++i)
    {
        _x[i].reserve(m.n_stacks());
        for (std::size_t k = 0; k < m.n_stacks(); ++k)
        {
            GRBVar var;
            std::ostringstream oss;

            oss << "x{" << i + 1 << "," << k + 1 << "}";
            var = _model->addVar(0, 1, 0, GRB_BINARY, oss.str());
            _x[i].push_back(var);
        }
    }

    // Minimize objective
    _model->set(GRB_IntAttr_ModelSense, GRB_MINIMIZE);

    // Fixed items constraint
    for (std::size_t i = 0; i < m.n_initial_items(); ++i)
    {
        for (std::size_t k = 0; k < m.n_stacks(); ++k)
        {
            double value = ((m.initial_position(i) == k) ? 1. : 0.);
            _x[i][k].set(GRB_DoubleAttr_LB, value);
            _x[i][k].set(GRB_DoubleAttr_UB, value);
        }
    }

    // Assignment constraint
    for (std::size_t i = 0; i < m.n_items(); ++i)
    {
        GRBLinExpr expr = 0;
        std::ostringstream oss;

        for (std::size_t k = 0; k < m.n_stacks(); ++k)
        {
            expr += _x[i][k];
        }

        oss << "assign{" << i + 1 << "}";
        _model->addConstr(expr, GRB_EQUAL, 1, oss.str());
    }

    // Capacity constraint
    if (m.n_items() > m.capacity())
    {
        for (std::size_t k = 0; k < m.n_stacks(); ++k)
        {
            GRBLinExpr expr = 0;
            std::ostringstream oss;

            for (std::size_t i = 0; i < m.n_items(); ++i)
            {
                expr += _x[i][k];
            }

            oss << "capacity{" << k + 1 << "}";
            _model->addConstr(expr, GRB_LESS_EQUAL, m.capacity(), oss.str());
        }
    }

    // Soft stacking constraint
    for (std::size_t i = 0; i < m.n_items(); ++i)
    {
        for (std::size_t j = 0; j < i; ++j)
        {
            if ((_allow_violations || m.s(i, j)) && m.r(i, j))
            {
                for (std::size_t k = 0; k < m.n_stacks(); ++k)
                {
                    std::ostringstream oss;
                    auto expr = (_x[i][k] + _x[j][k] <= 1 + _z[i]);

                    oss << "softstack{" << i + 1 << "," << j + 1 << "," << k + 1 << "}";
                    _model->addConstr(expr, oss.str());
                }
            }
        }
    }

    // Hard stacking constraint
    for (std::size_t i = 0; i < m.n_items(); ++i)
    {
        for (std::size_t j = 0; j < i; ++j)
        {
            if (!m.s(i, j))
            {
                for (std::size_t k = 0; k < m.n_stacks(); ++k)
                {
                    std::ostringstream oss;
                    oss << "hardstack{" << i + 1 << "," << j + 1 << "," << k + 1 << "}";

                    if (_allow_violations)
                    {
                        auto expr = (_x[i][k] + _x[j][k] <= 1 + _v[i]);
                        _model->addConstr(expr, oss.str());
                    }
                    else
                    {
                        auto expr = (_x[i][k] + _x[j][k] <= 1);
                        _model->addConstr(expr, oss.str());
                    }
                }
            }
        }
    }

    _model->update();
    _solution.initialize(m);
}

void StackingGUROBI::run()
{
    _model->optimize();
}

void StackingGUROBI::build_solution()
{
    for (std::size_t i = 0; i < _solution.n_items(); ++i)
    {
        for (std::size_t k = 0; k < _solution.n_stacks(); ++k)
        {
            if (_x[i][k].get(GRB_DoubleAttr_X) > 0.5)
            {
                _solution.push(_stacking_model, i, k);
            }
        }
    }

    // Check if the objective value matches
    if (is_feasible())
    {
        double expected = _solution.n_blocking_items();
        if (_allow_violations)
        {
            expected += _solution.n_items() * _solution.n_violating_items(_stacking_model);
        }

        if (std::abs(_model->get(GRB_DoubleAttr_ObjVal) - expected) > 1e-6)
        {
            throw std::runtime_error("StackingGUROBI::build_solution: objective values do not match");
        }
    }

    // Check if the blocking items match
    for (std::size_t i = 0; i < _solution.n_items(); ++i)
    {
        if ((_z[i].get(GRB_DoubleAttr_X) > 0.5) != (_solution.n_items_blocked_by(i) > 0))
        {
            throw std::runtime_error("StackingGUROBI::build_solution: blocking items do not match");
        }
    }
}

void StackingGUROBI::export_model(const char *filename)
{
    _model->write(filename);
}

void StackingGUROBI::export_solutions([[maybe_unused]] const char *filename) const
{
    throw std::runtime_error("StackingGUROBI::export_solutions: not supported");
}