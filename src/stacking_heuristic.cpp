/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stacking_heuristic.hpp"
#include <algorithm>
#include <cmath>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <tuple>

// Enable more strict (and very slow) debugging
#define DEBUG 0

StackingHeuristic::StackingHeuristic(std::mt19937 &rng)
    : _rng(rng),
      _n_repairs(0),
      _n_1_opt_moves(0),
      _n_2_opt_moves(0)
{
}

const StackingHeuristic::Model &StackingHeuristic::model() const
{
    return _model;
}

const StackingHeuristic::Solution &StackingHeuristic::solution() const
{
    return _solution;
}

std::size_t StackingHeuristic::n_repairs() const
{
    return _n_repairs;
}

std::size_t StackingHeuristic::n_1_opt_moves() const
{
    return _n_1_opt_moves;
}

std::size_t StackingHeuristic::n_2_opt_moves() const
{
    return _n_2_opt_moves;
}

void StackingHeuristic::set_model(const Model &model)
{
    _model = model;
}

void StackingHeuristic::set_repair(bool repair)
{
    _repair = repair;
}

void StackingHeuristic::set_allow_violations(bool allow_violations)
{
    _allow_violations = allow_violations;
}

void StackingHeuristic::set_debug(bool debug)
{
    _debug = debug;
}

void StackingHeuristic::construct()
{
    std::size_t n = _model.n_items() - _model.n_initial_items();
    std::vector<std::size_t> index(n);

    // Create a sequence nfix, nfix+1, ..., n-1
    std::iota(index.begin(), index.end(), _model.n_initial_items());

    // Sort indices
    std::sort(index.begin(), index.end(), _sort);

    // Reset the solution
    _solution.initialize(_model);

    _n_repairs = 0;

    // Select a stack for each item
    for (std::size_t i_ = 0; i_ < n; ++i_)
    {
        std::size_t i = index[i_];
        std::size_t k = _select(_model, _solution, i);

        if (k < _model.n_stacks())
        {
            _solution.push(_model, i, k);
        }
        else if (_repair)
        {
            ++_n_repairs;
            if (!repair(i) && _allow_violations)
            {
                // When repair fails and violations are allowed, selects a random available stack
                std::vector<std::size_t> candidates;
                for (std::size_t k = 0; k < _model.n_stacks(); ++k)
                {
                    if (_solution.height(k) < _model.capacity())
                    {
                        candidates.push_back(k);
                    }
                }

                if (candidates.empty())
                {
                    throw std::runtime_error("StackingHeuristic::construct: no slot available");
                }

                // Select stack with uniform distribution
                std::uniform_int_distribution<int> dist(0, candidates.size() - 1);
                int l = dist(_rng);
                std::size_t k = candidates[l];
                _solution.push(_model, i, k);
            }
        }
    }

    _n_1_opt_moves = 0;
    _n_2_opt_moves = 0;
}

bool StackingHeuristic::repair(std::size_t i)
{
    if (_debug)
    {
        std::clog << "  repair:"
                  << " n_blocking: " << _solution.n_blocking_items()
                  << " n_blocked: " << _solution.n_blocked_items() << std::endl;
    }

    // Save the original solution, the actual solution will represent the best one
    auto original_sol = _solution;

    // Minimize the number of blocking items, and to break ties, the number of blocked items
    std::size_t min_n_blocking = _model.n_items();
    std::size_t min_n_blocked = _model.n_items();
    std::size_t n_initial_items = _model.n_initial_items();

    // For each stack, try to move all items that are incompatible with item i
    for (std::size_t k1 = 0; k1 < _model.n_stacks(); ++k1)
    {
        std::vector<std::size_t> incompatible_items;

        // Get items incompatible with item i in stack k1
        for (auto j : _model.s_list(i))
        {
            if (i != j && original_sol.assignment(j) == k1)
            {
                incompatible_items.push_back(j);
            }
        }

        if (_debug)
        {
            // Check if the list of incompatible items is correct
            std::vector<std::size_t> incompatible_items_dbg;
            for (auto j : original_sol.items(k1))
            {
                if ((i > j && !_model.s(i, j)) || (i < j && !_model.s(j, i)))
                {
                    incompatible_items_dbg.push_back(j);
                }
            }

            if (incompatible_items != incompatible_items_dbg)
            {
                throw std::runtime_error("StackingHeuristic::repair: invalid item list");
            }

            // The reason there is no incompatible items while the construction failed must be that the stack is full
            if (incompatible_items.empty() && original_sol.height(k1) != _model.capacity())
            {
                original_sol.dump(std::clog);
                throw std::runtime_error("StackingHeuristic::repair: stack must be full");
            }
        }

        // Ensure there will be a slot available for item i when incompatible items are gone
        if (incompatible_items.empty())
        {
            continue;
        }

        // Try to move these items from k1 to a destination stack k2
        for (std::size_t k2 = 0; k2 < _model.n_stacks(); ++k2)
        {
            // The destination stack must have available slots
            if (k1 != k2 && original_sol.height(k2) + incompatible_items.size() <= _model.capacity())
            {
                // We modify a copy of the original solution
                auto try_sol = original_sol;
                bool feasible_stack = true;

                // Check feasibility of moving these items to k2
                for (auto j : incompatible_items)
                {
                    // Cannot move initial items
                    if (j < n_initial_items)
                    {
                        feasible_stack = false;
                        break;
                    }

                    try_sol.pop(_model, j);
                    if (try_sol.push_is_feasible(_model, j, k2))
                    {
                        try_sol.push(_model, j, k2);
                    }
                    else
                    {
                        feasible_stack = false;
                        break;
                    }
                }

                if (feasible_stack)
                {
                    // Here all incompatible items should be removed from stack k1
                    try_sol.push(_model, i, k1);

                    if (_debug)
                    {
                        // Check if items in stacks are compatible with each other
                        for (std::size_t i1 = 0; i1 < _model.n_items(); ++i1)
                        {
                            for (std::size_t i2 = 0; i2 < _model.n_items(); ++i2)
                            {
                                if (try_sol.assignment(i1) < _model.n_stacks() &&
                                    try_sol.assignment(i1) == try_sol.assignment(i2) &&
                                    ((i1 > i2 && !_model.s(i1, i2)) ||
                                     (i1 < i2 && !_model.s(i2, i1))))
                                {
                                    try_sol.dump(std::clog);
                                    throw std::runtime_error("StackingHeuristic::repair: candidate solution violates stacking constraints");
                                }
                            }
                        }

                        // Check if stack heights are below the limit
                        for (std::size_t k = 0; k < _model.n_stacks(); ++k)
                        {
                            if (try_sol.height(k) > _model.capacity())
                            {
                                try_sol.dump(std::clog);
                                throw std::runtime_error("StackingHeuristic::repair: candidate solution violates capacity constraints");
                            }
                        }
                    }

                    // Check whether this solution is better than the actual best one
                    if (try_sol.n_blocking_items() < min_n_blocking ||
                        (try_sol.n_blocking_items() == min_n_blocking &&
                         try_sol.n_blocked_items() < min_n_blocked))
                    {
                        _solution = try_sol;
                        min_n_blocking = _solution.n_blocking_items();
                        min_n_blocked = _solution.n_blocked_items();

                        if (_debug)
                        {
                            std::clog
                                << "  - n_blocking: " << min_n_blocking
                                << " n_blocked: " << min_n_blocked << std::endl;
                        }
                    }
                }
            }
        }
    }

    if (_debug)
    {
        // Could not repair
        if (min_n_blocking == _model.n_items())
        {
            std::clog << "    none" << std::endl;
        }
    }
    return min_n_blocking != _model.n_items();
}

bool StackingHeuristic::local_search(unsigned int depth, bool extended, bool only_exchange)
{
    if (_solution.n_blocking_items() == 0)
    {
        return false;
    }

    if (depth >= 1 && local_search_1_opt(extended))
    {
        ++_n_1_opt_moves;
        return true;
    }

    if (depth >= 2 && local_search_2_opt(extended, only_exchange))
    {
        ++_n_2_opt_moves;
        return true;
    }
    return false;
}

bool StackingHeuristic::local_search_1_opt(bool extended)
{
    std::size_t best_n_blocking = _solution.n_blocking_items();
    std::size_t best_n_blocked = _solution.n_blocked_items();
    std::vector<std::tuple<std::size_t, std::size_t>> best_list; // (i, k)

    if (_debug)
    {
        std::clog << "- 1-opt" << std::endl;
    }

    // Search for the best item to move
    for (std::size_t i = _model.n_initial_items(); i < _model.n_items(); ++i)
    {
        // If an item is neither blocking nor blocked, it will not change the objective
        if (_solution.n_items_blocked_by(i) > 0 || _solution.n_items_blocking(i) > 0)
        {
            // Search for the best destination stack
            for (std::size_t k = 0; k < _model.n_stacks(); ++k)
            {
                // Candidate moves must be valid and feasible
                if (k != _solution.assignment(i) && _solution.push_is_feasible(_model, i, k))
                {
                    std::size_t try_n_blocking = n_blocking_1_opt(_model, _solution, i, k);
                    std::size_t try_n_blocked = n_blocked_1_opt(_model, _solution, i, k);

                    if (_debug)
                    {
                        // Check if pop_n_blocking_diff() and pop_n_blocked_diff() compute properly
                        Solution try_sol = _solution;
                        try_sol.pop(_model, i);
                        try_sol.push(_model, i, k);

                        if (!try_sol.is_feasible(_model) ||
                            try_sol.n_blocking_items() != try_n_blocking ||
                            try_sol.n_blocked_items() != try_n_blocked)
                        {
                            _solution.dump(std::clog);
                            try_sol.dump(std::clog);
                            throw std::runtime_error("StackingHeuristic::local_search_1_opt: invalid solution");
                        }
                    }

                    if (try_n_blocking < best_n_blocking ||
                        (extended && try_n_blocking == best_n_blocking && try_n_blocked < best_n_blocked))
                    {
                        best_n_blocking = try_n_blocking;
                        best_n_blocked = try_n_blocked;
                        best_list.clear();
                        best_list.push_back({i, k});
                    }
                    else if (!best_list.empty() &&
                             try_n_blocking == best_n_blocking &&
                             (!extended || try_n_blocked == best_n_blocked))
                    {
                        best_list.push_back({i, k});
                    }
                }
            }
        }
    }

    if (_debug)
    {
        std::clog << "  candidates:";
        for (std::size_t l = 0; l < best_list.size(); ++l)
        {
            auto [i, k] = best_list[l];
            std::clog << " "
                      << i + 1 << ":"
                      << _solution.assignment(i) + 1 << ">"
                      << k + 1;
        }
        if (best_list.empty())
        {
            std::clog << " none";
        }
        std::clog << std::endl;

#if DEBUG // Very strict and slow debug
        // Attempt to make a candidate list with a naive but safe approach to check correctness
        std::size_t best_n_blocking_debug = _solution.n_blocking_items();
        std::size_t best_n_blocked_debug = _solution.n_blocked_items();
        std::vector<std::tuple<std::size_t, std::size_t>> best_list_debug; // (i, k)

        // Execute a (very expensive) naive search and compare candidate list
        for (std::size_t i = _model.n_initial_items(); i < _model.n_items(); ++i)
        {
            for (std::size_t k = 0; k < _model.n_stacks(); ++k)
            {
                if (k != _solution.assignment(i))
                {
                    Solution try_sol = _solution;
                    try_sol.pop(_model, i);
                    try_sol.push(_model, i, k);
                    if (try_sol.is_feasible(_model))
                    {
                        if (try_sol.n_blocking_items() < best_n_blocking_debug ||
                            (extended && try_sol.n_blocking_items() == best_n_blocking_debug && try_sol.n_blocked_items() < best_n_blocked_debug))
                        {
                            best_n_blocking_debug = try_sol.n_blocking_items();
                            best_n_blocked_debug = try_sol.n_blocked_items();
                            best_list_debug.clear();
                            best_list_debug.push_back({i, k});
                        }
                        else if (!best_list_debug.empty() &&
                                 try_sol.n_blocking_items() == best_n_blocking_debug &&
                                 (!extended || try_sol.n_blocked_items() == best_n_blocked_debug))
                        {
                            best_list_debug.push_back({i, k});
                        }
                    }
                }
            }
        }

        if (best_list_debug != best_list)
        {
            std::clog << "  debug candidates:";
            for (std::size_t l = 0; l < best_list_debug.size(); ++l)
            {
                auto [i, k] = best_list_debug[l];
                std::clog << " "
                          << i + 1 << ":"
                          << _solution.assignment(i) + 1 << ">"
                          << k + 1;
            }
            if (best_list_debug.empty())
            {
                std::clog << " none";
            }
            std::clog << std::endl;
            throw std::runtime_error("StackingHeuristic::local_search_1_opt: best candidate lists do not match");
        }
#endif
    }

    // Index of the choice in best_list
    int choice = -1;

    if (best_list.size() > 1)
    {
        // Select move with uniform distribution
        std::uniform_int_distribution<int> dist(0, best_list.size() - 1);
        choice = dist(_rng);
    }
    else if (best_list.size() == 1)
    {
        choice = 0;
    }

    if (choice >= 0)
    {
        auto [i, k] = best_list[choice];

        if (_debug)
        {
            std::clog << "  select: "
                      << i + 1 << ":"
                      << _solution.assignment(i) + 1 << ">"
                      << k + 1
                      << std::endl;
        }

        _solution.pop(_model, i);
        _solution.push(_model, i, k);

        if (_debug)
        {
            _solution.check(_model);
        }
        return true;
    }
    return false;
}

// Attempt to execute a 2-opt.
// This action is very redundant (called many times in local_search_2_opt) and requires a lot of arguments
// So we implement it here before local_search_2_opt
void try_2_opt(const StackingModel &model,
               const StackingSolution &sol,
               std::size_t i1,
               std::size_t k1,
               std::size_t i2,
               std::size_t k2,
               bool extended,
               std::size_t &best_n_blocking,
               std::size_t &best_n_blocked,
               std::vector<std::tuple<std::size_t, std::size_t, std::size_t, std::size_t>> &best_list,
               bool debug)
{
    if (!is_feasible_2_opt(model, sol, i1, k1, i2, k2))
    {
        return;
    }

    StackingSolution s = sol;
    s.pop(model, i1);
    s.pop(model, i2);
    s.push(model, i1, k1);
    s.push(model, i2, k2);

    if (debug)
    {
        if (!s.is_feasible(model))
        {
            sol.dump(std::clog);
            s.dump(std::clog);
            throw std::runtime_error("StackingHeuristic try_2_opt: invalid solution");
        }
    }

    if (s.n_blocking_items() < best_n_blocking ||
        (extended && s.n_blocking_items() == best_n_blocking && s.n_blocked_items() < best_n_blocked))
    {
        best_n_blocking = s.n_blocking_items();
        best_n_blocked = s.n_blocked_items();
        best_list.clear();
        best_list.push_back({i1, k1, i2, k2});
    }
    else if (!best_list.empty() &&
             s.n_blocking_items() == best_n_blocking &&
             (!extended || s.n_blocked_items() == best_n_blocked))
    {
        best_list.push_back({i1, k1, i2, k2});
    }
}

bool StackingHeuristic::local_search_2_opt(bool extended, bool only_exchange)
{
    std::size_t best_n_blocking = _solution.n_blocking_items();
    std::size_t best_n_blocked = _solution.n_blocked_items();
    std::vector<std::tuple<std::size_t, std::size_t, std::size_t, std::size_t>> best_list; // (i1, k1, i2, k2)

    if (_debug)
    {
        std::clog << "- 2-opt" << std::endl;
    }

    // Search for the best first item to move
    for (std::size_t i1 = _model.n_initial_items(); i1 < _model.n_items(); ++i1)
    {
        // Search for the best second item to move
        for (std::size_t i2 = _model.n_initial_items(); i2 < i1; ++i2)
        {
            // If no item is neither blocking nor blocked, it will not improve the objective
            if (_solution.n_items_blocked_by(i1) > 0 ||
                _solution.n_items_blocking(i1) > 0 ||
                _solution.n_items_blocked_by(i2) > 0 ||
                _solution.n_items_blocking(i2) > 0)
            {
                if (only_exchange)
                {
                    if (_solution.assignment(i1) != _solution.assignment(i2))
                    {
                        try_2_opt(_model,
                                  _solution,
                                  i1,
                                  _solution.assignment(i2),
                                  i2,
                                  _solution.assignment(i1),
                                  extended,
                                  best_n_blocking,
                                  best_n_blocked,
                                  best_list,
                                  _debug);
                    }
                }
                else
                {
                    // We assume that 1-opt has already been done.
                    // We seek for 2-opt moves that are not equivalent to two independent 1-opt moves.
                    // So only moves that have at least one stack in common are candidate.
                    // Therefore, candidate moves have to satisfy at least one of these equations:
                    //   1. s[i1] == s[i2]: same origin
                    //   2. k1 == k2: same destination
                    //   3. s[i1] == k2: origin of i1 is destination of i2
                    //   4. s[i2] == k1: origin of i2 is destination of i1

                    // Search for the best destination for i1
                    for (std::size_t k1 = 0; k1 < _model.n_stacks(); ++k1)
                    {
                        if (k1 != _solution.assignment(i1))
                        {
                            if (_solution.assignment(i1) != _solution.assignment(i2) &&
                                k1 != _solution.assignment(i2))
                            {
                                // Once s[i1] != s[i2] and k1 is fixed to a stack different from s[i2]
                                // there are only two non-redundant possibilities:

                                // k2 == k1
                                try_2_opt(_model,
                                          _solution,
                                          i1,
                                          k1,
                                          i2,
                                          k1,
                                          extended,
                                          best_n_blocking,
                                          best_n_blocked,
                                          best_list,
                                          _debug);

                                // k2 == s[i1]
                                try_2_opt(_model,
                                          _solution,
                                          i1,
                                          k1,
                                          i2,
                                          _solution.assignment(i1),
                                          extended,
                                          best_n_blocking,
                                          best_n_blocked,
                                          best_list,
                                          _debug);
                            }
                            else
                            {
                                // s[i1] == s[i2] or s[i2] == k1 (excluding k1 == s[i1])
                                for (std::size_t k2 = 0; k2 < _model.n_stacks(); ++k2)
                                {
                                    if (k2 != _solution.assignment(i2))
                                    {
                                        try_2_opt(_model,
                                                  _solution,
                                                  i1,
                                                  k1,
                                                  i2,
                                                  k2,
                                                  extended,
                                                  best_n_blocking,
                                                  best_n_blocked,
                                                  best_list,
                                                  _debug);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if (_debug)
    {
        std::clog << "  candidates:";
        for (std::size_t l = 0; l < best_list.size(); ++l)
        {
            auto [i1, k1, i2, k2] = best_list[l];
            std::clog << " "
                      << i1 + 1 << ":"
                      << _solution.assignment(i1) + 1 << ">"
                      << k1 + 1 << ","
                      << i2 + 1 << ":"
                      << _solution.assignment(i2) + 1 << ">"
                      << k2 + 1;
        }
        if (best_list.empty())
        {
            std::clog << " none";
        }
        std::clog << std::endl;

#if DEBUG // Very strict and slow debug
        // Attempt to make a candidate list with a naive but safe approach to check correctness
        std::size_t best_n_blocking_debug = _solution.n_blocking_items();
        std::size_t best_n_blocked_debug = _solution.n_blocked_items();
        std::vector<std::tuple<std::size_t, std::size_t, std::size_t, std::size_t>> best_list_debug; // (i1, k1, i2, k2)

        // Execute a (very expensive) naive search and compare candidate list
        for (std::size_t i1 = _model.n_initial_items(); i1 < _model.n_items(); ++i1)
        {
            for (std::size_t i2 = _model.n_initial_items(); i2 < i1; ++i2)
            {
                for (std::size_t k1 = 0; k1 < _model.n_stacks(); ++k1)
                {
                    if (k1 != _solution.assignment(i1))
                    {
                        for (std::size_t k2 = 0; k2 < _model.n_stacks(); ++k2)
                        {
                            if (k2 != _solution.assignment(i2))
                            {
                                Solution try_sol = _solution;
                                try_sol.pop(_model, i1);
                                try_sol.pop(_model, i2);
                                try_sol.push(_model, i1, k1);
                                try_sol.push(_model, i2, k2);
                                if (try_sol.is_feasible(_model))
                                {
                                    if (try_sol.n_blocking_items() < best_n_blocking_debug ||
                                        (extended && try_sol.n_blocking_items() == best_n_blocking_debug && try_sol.n_blocked_items() < best_n_blocked_debug))
                                    {
                                        best_n_blocking_debug = try_sol.n_blocking_items();
                                        best_n_blocked_debug = try_sol.n_blocked_items();
                                        best_list_debug.clear();
                                        best_list_debug.push_back({i1, k1, i2, k2});
                                    }
                                    else if (!best_list_debug.empty() &&
                                             try_sol.n_blocking_items() == best_n_blocking_debug &&
                                             (!extended || try_sol.n_blocked_items() == best_n_blocked_debug))
                                    {
                                        best_list_debug.push_back({i1, k1, i2, k2});
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if (best_list_debug != best_list)
        {
            std::clog << "  debug candidates:";
            for (std::size_t l = 0; l < best_list_debug.size(); ++l)
            {
                auto [i1, k1, i2, k2] = best_list_debug[l];
                std::clog << " "
                          << i1 + 1 << ":"
                          << _solution.assignment(i1) + 1 << ">"
                          << k1 + 1 << ","
                          << i2 + 1 << ":"
                          << _solution.assignment(i2) + 1 << ">"
                          << k2 + 1;
            }
            if (best_list_debug.empty())
            {
                std::clog << " none";
            }
            std::clog << std::endl;
            throw std::runtime_error("StackingHeuristic::local_search_2_opt: best candidate lists do not match");
        }
#endif
    }

    // Index of the choice in best_list
    int choice = -1;

    if (best_list.size() > 1)
    {
        // Select move with uniform distribution
        std::uniform_int_distribution<int> dist(0, best_list.size() - 1);
        choice = dist(_rng);
    }
    else if (best_list.size() == 1)
    {
        choice = 0;
    }

    if (choice >= 0)
    {
        auto [i1, k1, i2, k2] = best_list[choice];

        if (_debug)
        {
            std::clog << "  select: "
                      << i1 + 1 << ":"
                      << _solution.assignment(i1) + 1 << ">"
                      << k1 + 1 << ","
                      << i2 + 1 << ":"
                      << _solution.assignment(i2) + 1 << ">"
                      << k2 + 1
                      << std::endl;
        }

        _solution.pop(_model, i1);
        _solution.pop(_model, i2);
        _solution.push(_model, i1, k1);
        _solution.push(_model, i2, k2);

        if (_debug)
        {
            _solution.check(_model);
        }
        return true;
    }
    return false;
}

void StackingHeuristic::normalize_solution()
{
    _solution.normalize();
}

std::size_t _StackingHeuristic_GeoSelect::operator()(const Model &model, const Solution &sol, std::size_t i) const
{
    std::vector<std::size_t> candidates;
    std::size_t m = std::min(sol.n_used_stacks() + 1, model.n_stacks());
    std::size_t best_n_blocking = model.n_items();
    std::size_t best_n_blocked = model.n_items();
    std::size_t best_k = model.n_stacks();

    if (_debug)
    {
        std::clog << "- item: " << i + 1 << std::endl;
    }

    // Find best candidate stacks
    for (std::size_t k = 0; k < m; ++k)
    {
        if (sol.push_is_feasible(model, i, k))
        {
            std::size_t try_n_blocking = n_blocking_push(model, sol, i, k);
            std::size_t try_n_blocked = n_blocked_push(model, sol, i, k);

            if (_debug)
            {
                // Check if push_n_blocking_diff() and push_n_blocked_diff() compute properly
                Solution try_sol = sol;
                try_sol.push(model, i, k);

                if (try_sol.n_blocking_items() != try_n_blocking || try_sol.n_blocked_items() != try_n_blocked)
                {
                    sol.dump(std::clog);
                    try_sol.dump(std::clog);
                    throw std::runtime_error("GeoSelect: invalid solution");
                }
            }

            if (try_n_blocking < best_n_blocking ||
                (_extended && try_n_blocking == best_n_blocking &&
                 try_n_blocked < best_n_blocked))
            {
                candidates.clear();
                best_n_blocking = try_n_blocking;
                best_n_blocked = try_n_blocked;
            }

            if (try_n_blocking == best_n_blocking &&
                (!_extended || try_n_blocked == best_n_blocked) &&
                (_randomness > 0 || candidates.empty()))
            {
                candidates.push_back(k);
            }
        }
    }

    if (_debug)
    {
        std::clog << "  candidates:";
        for (auto k : candidates)
        {
            std::clog << " " << k + 1;
        }
        std::clog << std::endl;
    }

    // Allow stacking violations at construction step?
    if (_allow_violations && candidates.empty())
    {
        // No feasible choice, but we absolutely want to select a stack
        // Choose the leftmost stack
        for (std::size_t k = 0; k < model.n_stacks() && candidates.empty(); ++k)
        {
            // Selected stack must have available slots
            if (sol.height(k) < model.capacity())
            {
                candidates.push_back(k);
            }
        }

        if (_debug)
        {
            std::clog << "  candidates_violation:";
            for (auto k : candidates)
            {
                std::clog << " " << k + 1;
            }
            std::clog << std::endl;
        }
    }

    if (candidates.size() > 1)
    {
        if (_randomness < 1)
        {
            std::vector<double> p; // Probabilities
            double total = 1.e6;   // Scaling
            double pow_rand = std::pow(_randomness, candidates.size());

            // Compute the probabilities for each stack
            p.resize(candidates.size());

            for (std::size_t i = 0; i < candidates.size(); ++i)
            {
                p[i] = total * std::pow(_randomness, i) * (1. - _randomness) / (1. - pow_rand);
            }

            // Select stack with geometric distribution with finite support
            std::discrete_distribution<int> dist(p.begin(), p.end());
            int l = dist(_rng);
            best_k = candidates[l];
        }
        else
        {
            // Select stack with uniform distribution
            std::uniform_int_distribution<int> dist(0, candidates.size() - 1);
            int l = dist(_rng);
            best_k = candidates[l];
        }
    }
    else if (candidates.size() == 1)
    {
        // Only one candidate
        best_k = candidates.front();
    }

    if (_debug)
    {
        std::clog << "  select: ";
        if (best_k < model.n_stacks())
        {
            std::clog << best_k + 1;
        }
        else
        {
            std::clog << "none";
        }
        std::clog << std::endl;
    }

    return best_k;
}

std::size_t n_blocking_push(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k)
{
    std::size_t n = sol.n_blocking_items();
    bool i_is_blocking = false;

    // For all items j that conflict with i
    for (auto j : model.r_list(i))
    {
        if (k == sol.assignment(j))
        {
            // j is above i, and j is not yet blocking
            if (j > i && sol.n_items_blocked_by(j) == 0)
            {
                ++n;
            }
            // i is above j
            else if (i > j)
            {
                i_is_blocking = true;
            }
        }
    }

    return n + (i_is_blocking ? 1 : 0);
}

std::size_t n_blocked_push(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k)
{
    std::size_t n = sol.n_blocked_items();
    bool i_is_blocked = false;

    // For all items j that conflict with i
    for (auto j : model.r_list(i))
    {
        if (k == sol.assignment(j))
        {
            // i is above j, and j is not yet blocked
            if (i > j && sol.n_items_blocking(j) == 0)
            {
                ++n;
            }
            // j is above i
            else if (j > i)
            {
                i_is_blocked = true;
            }
        }
    }

    return n + (i_is_blocked ? 1 : 0);
}

std::size_t n_blocking_1_opt(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k)
{
    std::size_t n = n_blocking_push(model, sol, i, k);

    // For all items j that conflict with i
    for (auto j : model.r_list(i))
    {
        if (sol.assignment(i) == sol.assignment(j))
        {
            // j is above i, and i is the only blocked by j
            if (j > i && sol.n_items_blocked_by(j) == 1)
            {
                --n;
            }
        }
    }

    return n - (sol.n_items_blocked_by(i) > 0 ? 1 : 0);
}

std::size_t n_blocked_1_opt(const StackingModel &model, const StackingSolution &sol, std::size_t i, std::size_t k)
{
    std::size_t n = n_blocked_push(model, sol, i, k);

    // For all items j that conflict with i
    for (auto j : model.r_list(i))
    {
        if (sol.assignment(i) == sol.assignment(j))
        {
            // i is above j, and i is the only blocking by j
            if (i > j && sol.n_items_blocking(j) == 1)
            {
                --n;
            }
        }
    }

    return n - (sol.n_items_blocking(i) > 0 ? 1 : 0);
}

bool is_feasible_2_opt(const StackingModel &model, const StackingSolution &sol, std::size_t i1, std::size_t k1, std::size_t i2, std::size_t k2)
{
// We assume that the following conditions are met by construction in our algorithms
#if 0
    // Same item twice?
    if (i1 == i2)
    {
        return k1 == k2 && sol.push_is_feasible(model, i1, k1);
    }
    // items already in their destination stack
    else if (sol.assignment(i1) == k1 && sol.assignment(i2) == k2)
    {
        return true;
    }
    // item i1 already in its destination stack but not i2
    else if (sol.assignment(i1) == k1)
    {
        return sol.push_is_feasible(model, i2, k2);
    }
    // item i2 already in its destination stack but not i1
    else if (sol.assignment(i2) == k2)
    {
        return sol.push_is_feasible(model, i1, k1);
    }
    // moves are independent (i.e. k1 != k2 and s[i1] != s[i2] and s[i1] != k2 and s[i2] != k1)
    else if (k1 != k2 &&
             sol.assignment(i1) != sol.assignment(i2) &&
             sol.assignment(i1) != k2 &&
             sol.assignment(i2) != k1)
    {
        return sol.push_is_feasible(model, i1, k1) && sol.push_is_feasible(model, i2, k2);
    }
#endif

    // Here i1 != i2 and s[i1] != k1 and s[i2] != k2
    //  and at least one of these is satisfied:
    //   1. k1 == k2
    //   2. s[i1] == s[i2]
    //   3. s[i1] == k2
    //   4. s[i2] == k1

    // Check capacity of destinations
    // k1 == k2 and there is no place for 2 items
    // Capacity of k1 (one slot added if i2 was in k1)
    // Capacity of k2 (one slot added if i1 was in k2)
    if ((k1 == k2 && sol.height(k1) >= model.capacity() - 1) ||
        (sol.height(k1) >= model.capacity() + (k1 == sol.assignment(i2) ? 1 : 0)) ||
        (sol.height(k2) >= model.capacity() + (k2 == sol.assignment(i1) ? 1 : 0)))
    {
        return false;
    }

    // Case where k1 == k2: check if i1 and i2 can be stacked together
    if (k1 == k2 && ((i1 > i2 && !model.s(i1, i2)) || (i2 > i1 && !model.s(i2, i1))))
    {
        return false;
    }

    // Check pairwise stacking constraints for i1
    for (auto j : model.s_list(i1))
    {
        // If k1 == k2, then we already checked they are compatible
        // so here we exclude i2 from checking because it will be out of k1
        if (k1 == sol.assignment(j) && j != i2)
        {
            return false;
        }
    }

    // Check pairwise stacking constraints for i2
    for (auto j : model.s_list(i2))
    {
        // If k1 == k2, then we already checked they are compatible
        // so here we exclude i1 from checking because it will be out of k2
        if (k2 == sol.assignment(j) && j != i1)
        {
            return false;
        }
    }
    return true;
}