/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stacking_solution.hpp"
#include <algorithm>
#include <cmath>
#include <iomanip>
#include <sstream>
#include <stdexcept>

// On Unix, we can query the terminal size to display the solution properly.
// On other systems, we will assume that the maximum width is 80.
#if defined(__unix__) || defined(__unix)
#define UNIX 1
#endif

#if UNIX
#include <sys/ioctl.h>
#endif

std::string _base(int n, int b)
{
    // Base:                        10        16    36                  64
    static const char alphabet[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/";
    std::string r;

    while (n)
    {
        r += alphabet[n % b];
        n /= b;
    }
    return std::string(r.rbegin(), r.rend());
}

StackingSolution::StackingSolution(const Model &model)
{
    initialize(model);
}

void StackingSolution::initialize(const Model &model)
{
    _n_blocking = 0;
    _n_blocked = 0;
    _n_used_stacks = 0;

    _assignments.clear();
    _heights.clear();
    _n_items_blocking.clear();
    _n_items_blocked_by.clear();

    _assignments.resize(model.n_items(), model.n_stacks());
    _heights.resize(model.n_stacks(), 0);
    _n_items_blocking.resize(model.n_items(), 0);
    _n_items_blocked_by.resize(model.n_items(), 0);

    for (std::size_t i = 0; i < model.n_initial_items(); ++i)
    {
        push(model, i, model.initial_position(i));
    }
}

std::size_t StackingSolution::n_items() const
{
    return _assignments.size();
}

std::size_t StackingSolution::n_stacks() const
{
    return _heights.size();
}

std::size_t StackingSolution::n_blocking_items() const
{
    return _n_blocking;
}

std::size_t StackingSolution::n_blocked_items() const
{
    return _n_blocked;
}

std::size_t StackingSolution::n_used_stacks() const
{
    return _n_used_stacks;
}

std::size_t StackingSolution::n_violating_items(const Model &model) const
{
    std::size_t n_violating = 0;

    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        bool violating = false;
        if (_assignments[i] < model.n_stacks())
        {
            for (auto j : model.s_list(i))
            {
                if (_assignments[i] == _assignments[j] && i > j)
                {
                    violating = true;
                    break;
                }
            }
        }

        if (violating)
        {
            ++n_violating;
        }
    }

    return n_violating;
}

std::size_t StackingSolution::assignment(std::size_t i) const
{
    return _assignments[i];
}

const std::vector<std::size_t> &StackingSolution::assignments() const
{
    return _assignments;
}

std::size_t StackingSolution::n_items_blocking(std::size_t i) const
{
    return _n_items_blocking[i];
}

std::size_t StackingSolution::n_items_blocked_by(std::size_t i) const
{
    return _n_items_blocked_by[i];
}

std::size_t StackingSolution::height(std::size_t k) const
{
    return _heights[k];
}

bool StackingSolution::is_feasible(const Model &model) const
{
    // Check violation of capacity
    for (std::size_t k = 0; k < model.n_stacks(); ++k)
    {
        if (_heights[k] > model.capacity())
        {
            return false;
        }
    }

    // Check violation of pairwise stacking constraints
    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        if (_assignments[i] < model.n_stacks())
        {
            for (auto j : model.s_list(i))
            {
                if (_assignments[i] == _assignments[j] && i != j)
                {
                    return false;
                }
            }
        }
        else
        {
            return false;
        }
    }

    return true;
}

std::vector<std::size_t> StackingSolution::items(std::size_t k) const
{
    std::vector<std::size_t> c;

    for (std::size_t i = 0; i < n_items(); ++i)
    {
        if (_assignments[i] == k)
        {
            c.push_back(i);
        }
    }
    return c;
}

std::size_t StackingSolution::top_item(std::size_t k) const
{
    if (_heights[k] > 0)
    {
        for (std::size_t i = 0; i < n_items(); ++i)
        {
            if (_assignments[n_items() - 1 - i] == k)
            {
                return i;
            }
        }
    }
    return n_items();
}

std::size_t StackingSolution::bottom_item(std::size_t k) const
{
    if (_heights[k] > 0)
    {
        for (std::size_t i = 0; i < n_items(); ++i)
        {
            if (_assignments[i] == k)
            {
                return i;
            }
        }
    }
    return n_items();
}

void StackingSolution::push(const Model &model, std::size_t i, std::size_t k)
{
    // item already in the destination stack
    if (_assignments[i] == k)
    {
        return;
    }

    // item in another stack
    if (_assignments[i] < model.n_stacks())
    {
        pop(model, i);
    }

    _assignments[i] = k;

    if (_heights[k] == 0)
    {
        ++_n_used_stacks;
    }

    ++_heights[k];

    for (auto j : model.r_list(i))
    {
        if (k == _assignments[j])
        {
            // i arrives after j
            if (i > j)
            {
                // i is newly blocking by an item
                if (_n_items_blocked_by[i] == 0)
                {
                    ++_n_blocking;
                }

                // j is newly blocked
                if (_n_items_blocking[j] == 0)
                {
                    ++_n_blocked;
                }

                // j is a new item blocked by i
                ++_n_items_blocked_by[i];

                // i is a new item blocking j
                ++_n_items_blocking[j];
            }
            // j arrives after i
            else if (j > i)
            {
                // j is newly blocking by an item
                if (_n_items_blocked_by[j] == 0)
                {
                    ++_n_blocking;
                }

                // i is newly blocked
                if (_n_items_blocking[i] == 0)
                {
                    ++_n_blocked;
                }

                // i is a new item blocked by j
                ++_n_items_blocked_by[j];

                // j is a new item blocking i
                ++_n_items_blocking[i];
            }
        }
    }
}

void StackingSolution::pop(const Model &model, std::size_t i)
{
    std::size_t k = _assignments[i];

    // item already out
    if (k >= model.n_stacks())
    {
        return;
    }

    _assignments[i] = model.n_stacks();

    --_heights[k];

    if (_heights[k] == 0)
    {
        --_n_used_stacks;
    }

    for (auto j : model.r_list(i))
    {
        if (k == _assignments[j])
        {
            // i arrives after j
            if (i > j)
            {
                // j is no longer blocked by i
                --_n_items_blocked_by[i];

                // i is no longer blocking j
                --_n_items_blocking[j];

                // i is no longer blocking by an item
                if (_n_items_blocked_by[i] == 0)
                {
                    --_n_blocking;
                }

                // j is no longer blocked
                if (_n_items_blocking[j] == 0)
                {
                    --_n_blocked;
                }
            }
            // j arrives after i
            else if (j > i)
            {
                // i is no longer blocked by j
                --_n_items_blocked_by[j];

                // j is no longer blocking i
                --_n_items_blocking[i];

                // j is no longer blocking by an item
                if (_n_items_blocked_by[j] == 0)
                {
                    --_n_blocking;
                }

                // i is no longer blocked
                if (_n_items_blocking[i] == 0)
                {
                    --_n_blocked;
                }
            }
        }
    }
}

bool StackingSolution::push_is_feasible(const Model &model, std::size_t i, std::size_t k) const
{
    // Check capacity
    if (_heights[k] >= model.capacity())
    {
        return false;
    }

    // Check pairwise stacking constraints
    for (auto j : model.s_list(i))
    {
        if (k == _assignments[j])
        {
            return false;
        }
    }

    return true;
}

void StackingSolution::normalize()
{
    std::vector<std::size_t> index(n_stacks()), rindex(n_stacks()), h(_heights);

    for (std::size_t k = 0; k < n_stacks(); ++k)
    {
        index[k] = k;
    }

    std::sort(index.begin(), index.end(),
              [this](auto k, auto l) {
                  auto bottom_k = bottom_item(k);
                  auto bottom_l = bottom_item(l);
                  return bottom_k < bottom_l || (bottom_k == bottom_l && k < l);
              });

    for (std::size_t k = 0; k < n_stacks(); ++k)
    {
        rindex[index[k]] = k;
    }

    for (std::size_t i = 0; i < n_items(); ++i)
    {
        if (_assignments[i] < n_stacks())
        {
            _assignments[i] = rindex[_assignments[i]];
        }
    }

    // Heights need to be updated
    for (std::size_t k = 0; k < n_stacks(); ++k)
    {
        _heights[k] = h[index[k]];
    }
}

void StackingSolution::check(const Model &model) const
{
    std::size_t n_blocking_check = 0;                  // Number of blocking items
    std::size_t n_blocked_check = 0;                   // Number of blocked items
    std::vector<std::size_t> n_items_blocked_by_check; // n_items_blocked_by_check[i] > 0 if i is blocking
    std::vector<std::size_t> n_items_blocking_check;   // n_items_blocking_check[i] > 0 if i is blocked
    std::vector<bool> is_blocking_check;               // true if item i is blocking, false otherwise
    std::vector<bool> is_blocked_check;                // true if item i is blocked, false otherwise
    std::vector<std::size_t> heights_check;            // Stack heights
    std::size_t n_used_stacks_check = 0;

    // Check dimensions

    if (_assignments.size() != model.n_items() ||
        _n_items_blocking.size() != model.n_items() ||
        _n_items_blocked_by.size() != model.n_items())
    {
        throw std::runtime_error("StackingSolution::check: n_items do not match");
    }

    if (_heights.size() != model.n_stacks())
    {
        throw std::runtime_error("StackingSolution::check: n_stacks do not match");
    }

    // Check whether initial items are assigned to their initial stacks

    for (std::size_t i = 0; i < model.n_initial_items(); ++i)
    {
        if (_assignments[i] < model.n_stacks() &&
            _assignments[i] != model.initial_position(i))
        {
            throw std::runtime_error("StackingSolution::check: initial items not at initial position");
        }
    }

    // Check from scratch whether items are blocking/blocked

    n_items_blocked_by_check.resize(model.n_items(), 0);
    n_items_blocking_check.resize(model.n_items(), 0);

    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        // Not assigned items are ignored (incomplete solution)
        if (_assignments[i] < model.n_stacks())
        {
            for (std::size_t j = 0; j < i; ++j)
            {
                if (_assignments[i] == _assignments[j] && model.r(i, j))
                {
                    ++n_items_blocked_by_check[i];
                    ++n_items_blocking_check[j];
                }
            }
        }
    }

    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        if (n_items_blocked_by_check[i] != _n_items_blocked_by[i])
        {
            throw std::runtime_error("StackingSolution::check: n_items_blocked_by do not match");
        }

        if (n_items_blocking_check[i] != _n_items_blocking[i])
        {
            throw std::runtime_error("StackingSolution::check: n_items_blocking do not match");
        }
    }

    // Check stack heights

    heights_check.resize(model.n_stacks(), 0);

    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        if (_assignments[i] < model.n_stacks())
        {
            ++heights_check[_assignments[i]];
        }
    }

    for (std::size_t k = 0; k < model.n_stacks(); ++k)
    {
        if (heights_check[k] != _heights[k])
        {
            throw std::runtime_error("StackingSolution::check: heights do not match");
        }

        if (_heights[k] > 0)
        {
            ++n_used_stacks_check;
        }
    }

    if (n_used_stacks_check != _n_used_stacks)
    {
        throw std::runtime_error("StackingSolution::check: n_used_stacks do not match");
    }

    // Check objective values

    is_blocking_check.resize(model.n_items(), false);
    is_blocked_check.resize(model.n_items(), false);

    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        if (_assignments[i] < model.n_stacks())
        {
            for (std::size_t j = 0; j < i; ++j)
            {
                if (_assignments[i] == _assignments[j] && model.r(i, j))
                {
                    is_blocking_check[i] = true;
                    is_blocked_check[j] = true;
                }
            }
        }
    }

    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        if (is_blocking_check[i])
        {
            ++n_blocking_check;
        }

        if (is_blocked_check[i])
        {
            ++n_blocked_check;
        }
    }

    if (n_blocking_check != _n_blocking)
    {
        throw std::runtime_error("StackingSolution::check: n_blocking_items do not match");
    }

    if (n_blocked_check != _n_blocked)
    {
        throw std::runtime_error("StackingSolution::check: n_blocked_items do not match");
    }
}

void StackingSolution::print(std::ostream &os) const
{
    for (std::size_t i = 0; i < n_items(); ++i)
    {
        if (i > 0)
        {
            os << " ";
        }
        os << _assignments[i] + 1;
    }
}

void StackingSolution::print_layout(std::ostream &os, const std::string &separator) const
{
    unsigned int max_width = 80;
    unsigned int b[] = {10, 16, 36, 64};
    double ln_n = std::log(n_items()) + 1e-9; // Due to imprecision with floor, we add a epsilon

#if UNIX
    if (os.rdbuf() == std::cout.rdbuf() || os.rdbuf() == std::clog.rdbuf())
    {
        // Get the terminal width in number of characters (Unix only)
        struct winsize ws;
        ioctl(0, TIOCGWINSZ, &ws);
        max_width = ws.ws_col;
    }
#endif

    for (unsigned int l = 0; l < 4; ++l)
    {
        unsigned int digits = 1 + (unsigned int)(std::floor(ln_n / std::log(b[l])));
        unsigned int width = (digits + 1) * n_stacks() - 1;

        if (width <= max_width)
        {
            auto fn = [&](std::size_t i) { return _base(i + 1, b[l]); };
            print_layout(fn, separator, os);
            return;
        }
    }

    // Too large to write numbers, display only "#"
    print_layout([](std::size_t) { return "#"; }, "", os);
}

void StackingSolution::print_layout_d(const Model &model, std::ostream &os, const std::string &separator) const
{
    auto fn = [&](std::size_t i) {
        std::ostringstream oss;
        oss << model.departure(i);
        return oss.str();
    };
    print_layout(fn, separator, os);
}

void StackingSolution::print_layout_w(const Model &model, std::ostream &os, const std::string &separator) const
{
    auto fn = [&](std::size_t i) {
        std::ostringstream oss;
        oss << model.weight(i);
        return oss.str();
    };
    print_layout(fn, separator, os);
}

void StackingSolution::print_layout(std::function<std::string(std::size_t)> fn, const std::string &separator, std::ostream &os) const
{
    unsigned int w = 1, h = 0;
    std::vector<std::vector<std::string>> cfg(n_stacks());

    for (std::size_t k = 0; k < n_stacks(); ++k)
    {
        auto item_list = items(k);
        auto q = item_list.size();

        if (h < q)
        {
            h = q;
        }

        for (std::size_t l = 0; l < q; ++l)
        {
            std::string s = fn(item_list[l]);

            if (w < s.size())
            {
                w = s.size();
            }

            cfg[k].push_back(s);
        }
    }

    for (int l = h - 1; l >= 0; --l)
    {
        for (std::size_t k = 0; k < cfg.size(); ++k)
        {
            int q = cfg[k].size();

            if (k > 0)
            {
                os << separator;
            }

            if (l < q)
            {
                os << std::setw(w) << cfg[k][l];
            }
            else
            {
                os << std::setw(w) << " ";
            }
        }
        os << std::endl;
    }

    os << std::string(n_stacks() * w + (n_stacks() - 1) * separator.size(), '-') << std::endl;
}

void StackingSolution::dump(std::ostream &os) const
{
    os
        << "n_blocking = " << _n_blocking << std::endl
        << "n_blocked = " << _n_blocked << std::endl
        << "n_used_stacks = " << _n_used_stacks << std::endl;

    os << "assignments =";
    for (std::size_t i = 0; i < _assignments.size(); ++i)
    {
        os << " [" << i << "] " << _assignments[i];
    }
    os << std::endl;

    os << "heights =";
    for (std::size_t i = 0; i < _heights.size(); ++i)
    {
        os << " [" << i << "] " << _heights[i];
    }
    os << std::endl;

    os << "n_items_blocking =";
    for (std::size_t i = 0; i < _n_items_blocking.size(); ++i)
    {
        os << " [" << i << "] " << _n_items_blocking[i];
    }
    os << std::endl;

    os << "n_items_blocked_by =";
    for (std::size_t i = 0; i < _n_items_blocked_by.size(); ++i)
    {
        os << " [" << i << "] " << _n_items_blocked_by[i];
    }
    os << std::endl;
}

std::ostream &operator<<(std::ostream &os, const StackingSolution &sol)
{
    sol.print(os);
    return os;
}