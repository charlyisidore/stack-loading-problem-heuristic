/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "run_info.hpp"
#include "stacking_model.hpp"
#include <algorithm>
#include <chrono>
#include <cmath>
#include <fstream>
#include <iostream>
#include <iterator>
#include <nlohmann/json.hpp>
#include <numeric>
#include <stack>
#include <stdexcept>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#if USE_BOOST_GRAPH
#include <boost/graph/bron_kerbosch_all_cliques.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/undirected_graph.hpp>
#endif

/**
 * Compute the size of the longest increasing subsequence of a given sequence.
 */
std::size_t longest_increasing_subsequence(const std::vector<double> &X)
{
    int n = X.size();
    std::vector<int> P(n), M(n + 1);
    int L = 0;
    for (int i = 0; i < n; ++i)
    {
        int lo = 1, hi = L;
        while (lo <= hi)
        {
            int mid = std::ceil((lo + hi) / 2.);
            if (X[M[mid]] < X[i])
            {
                lo = mid + 1;
            }
            else
            {
                hi = mid - 1;
            }
        }
        int newL = lo;
        P[i] = M[newL - 1];
        M[newL] = i;
        if (newL > L)
        {
            L = newL;
        }
    }
    return L;
}

/**
 * Compute a lower bound based on the longest increasing subsequence and measure the time.
 */
std::tuple<std::size_t, double> benchmark_lower_bound_lis(const std::string &name, const std::vector<double> &X, std::size_t m, bool verbose, bool debug)
{
    if (debug)
    {
        std::clog << std::endl;
    }

    std::chrono::time_point<std::chrono::steady_clock> begin_time;
    begin_time = std::chrono::steady_clock::now();
    std::size_t lb = longest_increasing_subsequence(X);

    if (debug)
    {
        std::clog << "LIS(" << name << ") = " << lb << std::endl;
    }

    if (lb > m)
    {
        lb -= m;
    }
    else
    {
        lb = 0;
    }

    double time = std::chrono::duration<double>(std::chrono::steady_clock::now() - begin_time).count();
    if (verbose)
    {
        std::clog << "LB_lis(" << name << ") = " << lb << " (" << time << " s)" << std::endl;
    }
    return {lb, time};
}

#if USE_BOOST_GRAPH

/**
 * A graph representation.
 */
class IndexedGraph
{
public:
    typedef boost::undirected_graph<boost::property<boost::vertex_index_t, std::size_t>> Graph;
    typedef Graph::vertex_descriptor VertexDescriptor;

    IndexedGraph()
    {
    }

    IndexedGraph(std::size_t n_vertices)
    {
        for (std::size_t i = 0; i < n_vertices; ++i)
        {
            add_vertex(i);
        }
    }

    IndexedGraph(const IndexedGraph &g)
    {
        operator=(g);
    }

    IndexedGraph &operator=(const IndexedGraph &g)
    {
        boost::copy_graph(g._g, _g);
        for (auto [v, v_end] = boost::vertices(_g); v != v_end; ++v)
        {
            _v[boost::get(boost::vertex_index, _g)[*v]] = *v;
        }
        return *this;
    }

    VertexDescriptor add_vertex(std::size_t i)
    {
        auto v = boost::add_vertex(i, _g);
        _v.insert({i, v});
        return v;
    }

    auto add_edge(std::size_t i, std::size_t j)
    {
        return boost::add_edge(_v.at(i), _v.at(j), _g);
    }

    auto add_edge(VertexDescriptor u, VertexDescriptor v)
    {
        return boost::add_edge(u, v, _g);
    }

    void remove_vertex(std::size_t i)
    {
        auto v = _v.at(i);
        boost::clear_vertex(v, _g);
        boost::remove_vertex(v, _g);
    }

    void remove_vertex(VertexDescriptor v)
    {
        boost::clear_vertex(v, _g);
        boost::remove_vertex(v, _g);
    }

    std::size_t n_vertices() const
    {
        return boost::num_vertices(_g);
    }

    VertexDescriptor vertex(std::size_t i) const
    {
        return _v.at(i);
    }

    std::size_t index(VertexDescriptor v) const
    {
        return boost::get(boost::vertex_index, _g)[v];
    }

    auto edge(VertexDescriptor u, VertexDescriptor v) const
    {
        return boost::edge(u, v, _g);
    }

    auto lookup_edge(VertexDescriptor u, VertexDescriptor v) const
    {
        return boost::lookup_edge(u, v, _g);
    }

    std::size_t degree(std::size_t i) const
    {
        return boost::out_degree(_v.at(i), _g);
    }

    std::size_t degree(VertexDescriptor v) const
    {
        return boost::out_degree(v, _g);
    }

    auto vertices() const
    {
        return boost::vertices(_g);
    }

    auto adjacent_vertices(std::size_t i) const
    {
        return boost::adjacent_vertices(_v.at(i), _g);
    }

    auto adjacent_vertices(VertexDescriptor v) const
    {
        return boost::adjacent_vertices(v, _g);
    }

    auto edges(std::size_t i) const
    {
        return boost::out_edges(_v.at(i), _g);
    }

    auto edges(VertexDescriptor v) const
    {
        return boost::out_edges(v, _g);
    }

    const Graph &graph() const
    {
        return _g;
    }

    Graph &graph()
    {
        return _g;
    }

    void write_graphviz(std::ostream &os = std::cout) const
    {
        boost::write_graphviz(os, _g, [this](auto &s, auto v) { s << "[label=\"" << index(v) + 1 << "\"]"; });
    }

private:
    Graph _g;
    std::unordered_map<std::size_t, VertexDescriptor> _v;
};

/**
 * Write a conflict graph to a file.
 */
void export_conflict_graph(const std::string &name, const std::string &filename, const IndexedGraph &g, bool verbose)
{
    if (!filename.empty())
    {
        if (verbose)
        {
            std::clog << "> Export " << name << " conflict graph to '" << filename << "'..." << std::endl;
        }
        std::ofstream file(filename);
        if (!file.is_open())
        {
            throw std::runtime_error("Error opening file");
        }
        g.write_graphviz(file);
        file.close();
    }
}

/**
 * A visitation function for the maximum clique problem.
 */
template <typename Container>
class MaximumCliqueVisitor
{
public:
    MaximumCliqueVisitor(Container &clique)
        : _clique(clique) {}

    template <typename Clique, typename Graph>
    inline void clique(const Clique &c, [[maybe_unused]] const Graph &g)
    {
        if (std::size(c) > std::size(_clique))
        {
            _clique = Container();
            std::copy(std::begin(c), std::end(c), std::inserter(_clique, std::begin(_clique)));
        }
    }

private:
    Container &_clique;
};

/**
 * Return a visitation function for the maximum clique problem.
 */
template <typename Container>
MaximumCliqueVisitor<Container> find_maximum_clique(Container &c)
{
    return MaximumCliqueVisitor<Container>(c);
}

/**
 * Compute a lower bound based on conflict graphs (Bron-Kerbosch).
 */
std::tuple<std::size_t, std::size_t> lower_bound_bron_kerbosch(const IndexedGraph &g, std::size_t m, bool debug = false)
{
    std::size_t lb = 0;
    std::size_t it = 0;
    bool lb_is_zero = true;

    if (debug)
    {
        std::clog << std::endl;
    }

    for (auto [v, v_end] = g.vertices(); v != v_end; ++v)
    {
        if (g.degree(*v) > m)
        {
            if (debug)
            {
                std::clog << "Found degree " << g.degree(*v) << " (vertex " << g.index(*v) + 1 << ")" << std::endl;
            }
            lb_is_zero = false;
            break;
        }
    }

    if (lb_is_zero)
    {
        return {0, 0};
    }

    IndexedGraph gc = g;

    while (gc.n_vertices() > 0)
    {
        // Get the largest clique
        std::vector<IndexedGraph::VertexDescriptor> clique;
        boost::bron_kerbosch_all_cliques(gc.graph(), find_maximum_clique(clique));
        ++it;

        if (clique.empty())
        {
            break;
        }

        if (debug)
        {
            std::clog << "Clique [" << it << "] (" << clique.size() << ") =";
        }

        // Remove vertices of the clique
        for (auto v : clique)
        {
            if (debug)
            {
                std::clog << " " << gc.index(v) + 1;
            }
            gc.remove_vertex(v);
        }

        if (debug)
        {
            std::clog << std::endl;
        }

        // Compute LB_k = sum_{l=1}^{k} |C_l| - k * m
        if (clique.size() > m)
        {
            lb += clique.size() - m;
        }
        else
        {
            break;
        }

        if (debug)
        {
            std::clog << "LB = " << lb << std::endl;
        }
    }
    return {lb, it};
}

/**
 * Compute the largest clique of a graph using a custom algorithm.
 */
template <class Graph>
std::vector<IndexedGraph::VertexDescriptor> largest_clique(const Graph &g)
{
    typedef typename Graph::vertex_descriptor Vertex;
    typedef std::size_t Index;
    typedef std::vector<Vertex> VertexVector;
    typedef std::vector<Index> IndexVector;
    typedef std::tuple<IndexVector, IndexVector, IndexVector> Tuple;

    // Largest clique
    IndexVector C_max;

    // Ordered list of vertices
    VertexVector V;

    // Mapping between vertices and indices
    std::unordered_map<Vertex, Index> I;

    // Ordered adjacency lists for std::set_intersection(...)
    std::vector<IndexVector> N;

    // Incidence matrix for O(1) lookup
    std::vector<std::vector<bool>> E;

    // Solution space to explore (stack of (C, P, X) tuples)
    std::stack<Tuple, std::vector<Tuple>> Q;

    // Build an ordered list of vertices
    {
        auto [V_begin, V_end] = boost::vertices(g);
        V.assign(V_begin, V_end);

        // The order of vertices impacts the performance of the algorithm
        // We also need to order the vertices for using std::set_intersection(...)
        // This function orders vertices by increasing degree
        std::stable_sort(std::begin(V), std::end(V), [&](auto u, auto v) {
            return boost::degree(u, g) < boost::degree(v, g);
        });
    }

    // Map vertices to their respective indices
    for (Index i = 0; i < V.size(); ++i)
    {
        auto v = V[i];
        I[v] = i;
    }

    // Build adjacency lists and incidence matrix
    N.resize(V.size());
    E.resize(V.size(), std::vector<bool>(V.size(), false));
    for (Index i = 0; i < V.size(); ++i)
    {
        auto v = V[i];

        // Get adjacent vertices of v
        for (auto [u, u_end] = boost::adjacent_vertices(v, g); u != u_end; ++u)
        {
            auto j = I[*u];
            N[i].push_back(j);
            E[i][j] = true;
        }

        // Sort the adjacency list
        std::sort(std::begin(N[i]), std::end(N[i]));
    }

    // Create the first P = [0, ..., n-1]
    IndexVector P_init(V.size());
    std::iota(std::begin(P_init), std::end(P_init), 0);

    // Initialize the first state
    Q.push({{}, std::move(P_init), {}});

    // Create P', X'
    IndexVector P_new;
    IndexVector X_new;

    // While there is remaining solution space to explore
    while (!Q.empty())
    {
        // Current clique C, candidate vertices P, excluded vertices X
        auto [C, P, X] = std::move(Q.top());
        Q.pop();

        // If the size of the clique plus candidate vertices is not larger than
        // our largest clique C_max, i.e. |C| + |P| <= |C_max|, then all cliques
        // in this solution space are smaller than |C_max|
        if (C.size() + P.size() <= C_max.size())
        {
            continue;
        }

        // Try adding each candidate vertex to the clique
        // P <- P \ {v} at the end of each iteration
        for (auto v = std::begin(P); v != std::end(P); v = P.erase(v))
        {
            // Neighbors of v
            const auto &N_v = N[*v];

            // Reuse P' and X', saves CPU time
            P_new.clear();
            X_new.clear();
            P_new.reserve(std::min(P.size(), N_v.size()));
            X_new.reserve(std::min(X.size(), N_v.size()));

            // C' <- C ⋃ {v}
            // Use the existing C to avoid copy, saves CPU time
            C.push_back(*v);

            // P' <- P ⋂ N(v)
            std::set_intersection(
                std::begin(P), std::end(P),
                std::begin(N_v), std::end(N_v),
                std::back_inserter(P_new));

            // X' <- X ⋂ N(v)
            std::set_intersection(
                std::begin(X), std::end(X),
                std::begin(N_v), std::end(N_v),
                std::back_inserter(X_new));

            // When P' and X' are empty, all vertices are visited
            if (P_new.empty() && X_new.empty())
            {
                // C' is a maximal clique
                if (C.size() > C_max.size())
                {
                    C_max = C;
                }
            }
            // Prune if |C'| + |P'| <= |C_max|
            else if (C.size() + P_new.size() > C_max.size())
            {
                // If we find a vertex in X' connected to all vertices in P',
                // any largest clique will be discovered in another branch
                bool prune = false;
                for (auto vX : X_new)
                {
                    // Neighborhood of vX
                    const auto &N_vX = N[vX];

                    // If the degree of vX is less than |P'|, it cannot be connected
                    // to all vertices in P'
                    if (N_vX.size() < P_new.size())
                    {
                        continue;
                    }

                    prune = true;
                    for (auto vP : P_new)
                    {
                        // Check whether vX is adjacent to vP
                        if (!E[vX][vP])
                        {
                            // vX not adjacent to vP, so not connected to all vertices in P'
                            prune = false;
                            break;
                        }
                    }

                    if (prune)
                    {
                        break;
                    }
                }

                if (!prune)
                {
                    // Add new state to the list
                    Q.emplace(C, P_new, X_new);
                }
            }

            // C <- C' \ {v}
            // Restore C
            C.pop_back();

            // X <- X ⋃ {v}
            X.push_back(*v);
        }
    }

    // Map indices to vertices and return the clique
    VertexVector clique;
    clique.reserve(C_max.size());
    for (auto i : C_max)
    {
        clique.push_back(V[i]);
    }
    return clique;
}

/**
 * Compute a lower bound based on conflict graphs (custom algorithm).
 */
std::tuple<std::size_t, std::size_t> lower_bound_graph(const IndexedGraph &g, std::size_t m, bool debug = false)
{
    std::size_t lb = 0;
    std::size_t it = 0;
    bool lb_is_zero = true;

    if (debug)
    {
        std::clog << std::endl;
    }

    for (auto [v, v_end] = g.vertices(); v != v_end; ++v)
    {
        if (g.degree(*v) > m)
        {
            if (debug)
            {
                std::clog << "Found degree " << g.degree(*v) << " (vertex " << g.index(*v) + 1 << ")" << std::endl;
            }
            lb_is_zero = false;
            break;
        }
    }

    if (lb_is_zero)
    {
        return {0, 0};
    }

    IndexedGraph gc = g;

    while (gc.n_vertices() > 0)
    {
        // Get the largest clique
        auto clique = largest_clique(gc.graph());
        ++it;

        if (clique.empty())
        {
            break;
        }

        if (debug)
        {
            std::clog << "Clique [" << it << "] (" << clique.size() << ") =";
        }

        // Remove vertices of the clique
        for (auto v : clique)
        {
            if (debug)
            {
                std::clog << " " << gc.index(v) + 1;
            }
            gc.remove_vertex(v);
        }

        if (debug)
        {
            std::clog << std::endl;
        }

        // Compute LB_k = sum_{l=1}^{k} |C_l| - k * m
        if (clique.size() > m)
        {
            lb += clique.size() - m;
        }
        else
        {
            break;
        }

        if (debug)
        {
            std::clog << "LB = " << lb << std::endl;
        }
    }
    return {lb, it};
}

/**
 * Compute a lower bound based on graphs (Bron-Kerbosch) and measure the time.
 */
std::tuple<std::size_t, std::size_t, double> benchmark_lower_bound_bron_kerbosch(const std::string &name, const IndexedGraph &g, std::size_t m, bool verbose, bool debug = false)
{
    std::chrono::time_point<std::chrono::steady_clock> begin_time;
    begin_time = std::chrono::steady_clock::now();
    auto [lb, it] = lower_bound_bron_kerbosch(g, m, debug);
    double time = std::chrono::duration<double>(std::chrono::steady_clock::now() - begin_time).count();
    if (verbose)
    {
        std::clog << "LB_graph(" << name << ") = " << lb << " (" << it << " iterations, " << time << " s)" << std::endl;
    }
    return {lb, it, time};
}

/**
 * Compute a lower bound based on graphs (custom algorithm) and measure the time.
 */
std::tuple<std::size_t, std::size_t, double> benchmark_lower_bound_graph(const std::string &name, const IndexedGraph &g, std::size_t m, bool verbose, bool debug = false)
{
    std::chrono::time_point<std::chrono::steady_clock> begin_time;
    begin_time = std::chrono::steady_clock::now();
    auto [lb, it] = lower_bound_graph(g, m, debug);
    double time = std::chrono::duration<double>(std::chrono::steady_clock::now() - begin_time).count();
    if (verbose)
    {
        std::clog << "LB_(" << name << ") = " << lb << " (" << it << " iterations, " << time << " s)" << std::endl;
    }
    return {lb, it, time};
}

#endif

/**
 * Display properties of the stacking problem.
 */
void run_info(const dopt::Map &args)
{
    StackingModel model;
    std::ifstream instance_file;
    std::string instance_filename;
    std::string s_graph_filename;
    std::string r_graph_filename;
    std::string c_graph_filename;
    bool debug = false;
    bool verbose = true;

    // Read command line options
    instance_filename = args[1].str();
    if (args["export-s-graph"])
    {
        s_graph_filename = args["export-s-graph"].str();
    }
    if (args["export-r-graph"])
    {
        r_graph_filename = args["export-r-graph"].str();
    }
    if (args["export-c-graph"])
    {
        c_graph_filename = args["export-c-graph"].str();
    }
    debug = args["debug"];
    verbose = !args["quiet"];

    if (verbose)
    {
        // Print command line options
        std::clog << "Options:" << std::endl
                  << "  instance_filename = '" << instance_filename << "'" << std::endl
                  << "  s_graph_filename  = '" << s_graph_filename << "'" << std::endl
                  << "  r_graph_filename  = '" << r_graph_filename << "'" << std::endl
                  << "  c_graph_filename  = '" << c_graph_filename << "'" << std::endl
                  << "  debug             = " << debug << std::endl
                  << std::endl;
    }

    // Read the instance
    if (verbose)
    {
        std::clog << "> Read instance..." << std::endl;
    }
    instance_file.open(instance_filename);
    if (!instance_file.is_open())
    {
        throw std::runtime_error("main: cannot open file");
    }
    model.read(instance_file);
    instance_file.close();

    if (verbose)
    {
        std::clog << "> Analyze..." << std::endl;
    }

    std::size_t lb_s_lis = 0, lb_r_lis = 0;
    double lb_s_lis_time = 0, lb_r_lis_time = 0;

    if (!model.is_arbitrary())
    {
        if (verbose)
        {
            std::clog << std::endl;
        }
        std::tie(lb_s_lis, lb_s_lis_time) = benchmark_lower_bound_lis("G_s", model.weights(), model.n_stacks(), verbose, debug);
        std::tie(lb_r_lis, lb_r_lis_time) = benchmark_lower_bound_lis("G_r", model.departures(), model.n_stacks(), verbose, debug);
    }

#if USE_BOOST_GRAPH

    IndexedGraph G_s(model.n_items()), G_r(model.n_items()), G_c(model.n_items());

    for (std::size_t i = 0; i < model.n_items(); ++i)
    {
        for (std::size_t j = 0; j < i; ++j)
        {
            if (!model.s(i, j))
            {
                G_s.add_edge(i, j);
            }
            if (model.r(i, j))
            {
                G_r.add_edge(i, j);
            }
            if (!model.s(i, j) || model.r(i, j))
            {
                G_c.add_edge(i, j);
            }
        }
    }

    export_conflict_graph("hard", s_graph_filename, G_s, verbose);
    export_conflict_graph("soft", r_graph_filename, G_r, verbose);
    export_conflict_graph("combined", c_graph_filename, G_c, verbose);

    if (verbose)
    {
        std::clog << std::endl;
    }

    auto [lb_s_graph, lb_s_graph_it, lb_s_graph_time] = benchmark_lower_bound_graph("G_s", G_s, model.n_stacks(), verbose, debug);
    auto [lb_r_graph, lb_r_graph_it, lb_r_graph_time] = benchmark_lower_bound_graph("G_r", G_r, model.n_stacks(), verbose, debug);
    auto [lb_c_graph, lb_c_graph_it, lb_c_graph_time] = benchmark_lower_bound_graph("G_c", G_c, model.n_stacks(), verbose, debug);

    // auto [lb_s_graph, lb_s_graph_it, lb_s_graph_time] = benchmark_lower_bound_bron_kerbosch("G_s", G_s, model.n_stacks(), verbose, debug);
    // auto [lb_r_graph, lb_r_graph_it, lb_r_graph_time] = benchmark_lower_bound_bron_kerbosch("G_r", G_r, model.n_stacks(), verbose, debug);
    // auto [lb_c_graph, lb_c_graph_it, lb_c_graph_time] = benchmark_lower_bound_bron_kerbosch("G_c", G_c, model.n_stacks(), verbose, debug);

    if (verbose)
    {
        std::clog << std::endl;
    }
#endif

    // Output data
    nlohmann::json data = nlohmann::json::object();

    if (!model.is_arbitrary())
    {
        data["lb_s_lis"] = lb_s_lis;
        data["lb_r_lis"] = lb_r_lis;
        data["lb_s_lis_time"] = lb_s_lis_time;
        data["lb_r_lis_time"] = lb_r_lis_time;
    }

#if USE_BOOST_GRAPH
    data["lb_s_graph"] = lb_s_graph;
    data["lb_r_graph"] = lb_r_graph;
    data["lb_c_graph"] = lb_c_graph;
    data["lb_s_graph_iterations"] = lb_s_graph_it;
    data["lb_r_graph_iterations"] = lb_r_graph_it;
    data["lb_c_graph_iterations"] = lb_c_graph_it;
    data["lb_s_graph_time"] = lb_s_graph_time;
    data["lb_r_graph_time"] = lb_r_graph_time;
    data["lb_c_graph_time"] = lb_c_graph_time;
#endif

    if (verbose)
    {
        std::cout << data.dump(2) << std::endl;
    }
    else
    {
        std::cout << data << std::endl;
    }
}