/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stacking_scip.hpp"
#include "scip_exception.hpp"
#include <cstdio>
#include <iostream>
#include <scip/scipdefplugins.h>
#include <sstream>
#include <stdexcept>

StackingSCIP::StackingSCIP()
    : _scip(nullptr)
{
    SCIP_CALL_EXC(SCIPcreate(&_scip));
    SCIP_CALL_EXC(SCIPincludeDefaultPlugins(_scip));
}

StackingSCIP::~StackingSCIP()
{
    SCIPfree(&_scip);
}

SCIP_STATUS StackingSCIP::status() const
{
    return SCIPgetStatus(_scip);
}

bool StackingSCIP::is_optimal() const
{
    return SCIPgetStatus(_scip) == SCIP_STATUS_OPTIMAL;
}

bool StackingSCIP::is_feasible() const
{
    return SCIPgetBestSol(_scip) != nullptr;
}

const StackingSCIP::Solution &StackingSCIP::solution() const
{
    return _solution;
}

void StackingSCIP::set_bool_param(const char *name, bool value)
{
    SCIP_CALL_EXC(SCIPsetBoolParam(_scip, name, value));
}

void StackingSCIP::set_int_param(const char *name, int value)
{
    SCIP_CALL_EXC(SCIPsetIntParam(_scip, name, value));
}

void StackingSCIP::set_long_int_param(const char *name, long long value)
{
    SCIP_CALL_EXC(SCIPsetLongintParam(_scip, name, value));
}

void StackingSCIP::set_real_param(const char *name, double value)
{
    SCIP_CALL_EXC(SCIPsetRealParam(_scip, name, value));
}

void StackingSCIP::set_log_file(const char *filename)
{
    SCIPsetMessagehdlrLogfile(_scip, filename);
}

void StackingSCIP::set_model(const Model &model)
{
    _model = model;
}

void StackingSCIP::set_allow_violations(bool allow)
{
    _allow_violations = allow;
}

void StackingSCIP::set_quiet()
{
    SCIPmessagehdlrSetQuiet(SCIPgetMessagehdlr(_scip), true);
}

void StackingSCIP::set_debug(bool debug)
{
    _debug = debug;
}

void StackingSCIP::build_model()
{
    SCIP_CALL_EXC(SCIPcreateProbBasic(_scip, "stacking"));

    // Minimize the number of blocking items
    SCIP_CALL_EXC(SCIPsetObjsense(_scip, SCIP_OBJSENSE_MINIMIZE));
    SCIP_CALL_EXC(SCIPsetObjIntegral(_scip));

    // Blocking items variables
    _z.resize(_model.n_items());
    for (std::size_t i = 0; i < _model.n_items(); ++i)
    {
        SCIP_VAR *var;
        std::ostringstream oss;

        oss << "z{" << i + 1 << "}";
        SCIP_CALL_EXC(SCIPcreateVarBasic(_scip,
                                         &var,
                                         oss.str().c_str(),
                                         0.,
                                         1.,
                                         1.,
                                         SCIP_VARTYPE_BINARY));
        SCIP_CALL_EXC(SCIPaddVar(_scip, var));
        _z[i] = var;
    }

    if (_allow_violations)
    {
        // Violating items variables
        _v.resize(_model.n_items());
        for (std::size_t i = 0; i < _model.n_items(); ++i)
        {
            SCIP_VAR *var;
            std::ostringstream oss;

            oss << "v{" << i + 1 << "}";
            SCIP_CALL_EXC(SCIPcreateVarBasic(_scip,
                                             &var,
                                             oss.str().c_str(),
                                             0.,
                                             1.,
                                             _model.n_items(),
                                             SCIP_VARTYPE_BINARY));
            SCIP_CALL_EXC(SCIPaddVar(_scip, var));
            _v[i] = var;
        }
    }

    // Assignment variables
    _x.resize(_model.n_items(), std::vector<SCIP_VAR *>(_model.n_stacks()));
    for (std::size_t i = 0; i < _model.n_items(); ++i)
    {
        for (std::size_t k = 0; k < _model.n_stacks(); ++k)
        {
            SCIP_VAR *var;
            std::ostringstream oss;

            oss << "x{" << i + 1 << "," << k + 1 << "}";
            SCIP_CALL_EXC(SCIPcreateVarBasic(_scip,
                                             &var,
                                             oss.str().c_str(),
                                             0.,
                                             1.,
                                             0.,
                                             SCIP_VARTYPE_BINARY));
            SCIP_CALL_EXC(SCIPaddVar(_scip, var));
            _x[i][k] = var;
        }
    }

    // Fixed items constraint
    for (std::size_t i = 0; i < _model.n_initial_items(); ++i)
    {
        for (std::size_t k = 0; k < _model.n_stacks(); ++k)
        {
            double value = ((_model.initial_position(i) == k) ? 1. : 0.);
            SCIP_Bool infeasible;
            SCIP_Bool fixed;

            SCIP_CALL_EXC(SCIPfixVar(_scip, _x[i][k], value, &infeasible, &fixed));
        }
    }

    // Assignment constraint
    for (std::size_t i = 0; i < _model.n_items(); ++i)
    {
        SCIP_CONS *cons;
        std::ostringstream oss;

        oss << "assign{" << i + 1 << "}";
        SCIP_CALL_EXC(SCIPcreateConsBasicLinear(_scip,
                                                &cons,
                                                oss.str().c_str(),
                                                0,
                                                nullptr,
                                                nullptr,
                                                1.,
                                                1.));
        for (std::size_t k = 0; k < _model.n_stacks(); ++k)
        {
            SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _x[i][k], 1.));
        }
        SCIP_CALL_EXC(SCIPaddCons(_scip, cons));
        SCIP_CALL_EXC(SCIPreleaseCons(_scip, &cons));
    }

    // Capacity constraint
    if (_model.n_items() > _model.capacity())
    {
        for (std::size_t k = 0; k < _model.n_stacks(); ++k)
        {
            SCIP_CONS *cons;
            std::ostringstream oss;

            oss << "capacity{" << k + 1 << "}";
            SCIP_CALL_EXC(SCIPcreateConsBasicLinear(_scip,
                                                    &cons,
                                                    oss.str().c_str(),
                                                    0,
                                                    nullptr,
                                                    nullptr,
                                                    -SCIPinfinity(_scip),
                                                    _model.capacity()));
            for (std::size_t i = 0; i < _model.n_items(); ++i)
            {
                SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _x[i][k], 1.));
            }
            SCIP_CALL_EXC(SCIPaddCons(_scip, cons));
            SCIP_CALL_EXC(SCIPreleaseCons(_scip, &cons));
        }
    }

    // Soft stacking constraint
    for (std::size_t i = 0; i < _model.n_items(); ++i)
    {
        for (std::size_t j = 0; j < i; ++j)
        {
            if ((_allow_violations || _model.s(i, j)) && _model.r(i, j))
            {
                for (std::size_t k = 0; k < _model.n_stacks(); ++k)
                {
                    SCIP_CONS *cons;
                    std::ostringstream oss;

                    oss << "softstack{" << i + 1 << "," << j + 1 << "," << k + 1 << "}";
                    SCIP_CALL_EXC(SCIPcreateConsBasicLinear(_scip,
                                                            &cons,
                                                            oss.str().c_str(),
                                                            0,
                                                            nullptr,
                                                            nullptr,
                                                            -SCIPinfinity(_scip),
                                                            1.));
                    SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _x[i][k], 1.));
                    SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _x[j][k], 1.));
                    SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _z[i], -1.));
                    SCIP_CALL_EXC(SCIPaddCons(_scip, cons));
                    SCIP_CALL_EXC(SCIPreleaseCons(_scip, &cons));
                }
            }
        }
    }

    // Hard stacking constraint
    for (std::size_t i = 0; i < _model.n_items(); ++i)
    {
        for (std::size_t j = 0; j < i; ++j)
        {
            if (!_model.s(i, j))
            {
                for (std::size_t k = 0; k < _model.n_stacks(); ++k)
                {
                    SCIP_CONS *cons;
                    std::ostringstream oss;

                    oss << "hardstack{" << i + 1 << "," << j + 1 << "," << k + 1 << "}";
                    SCIP_CALL_EXC(SCIPcreateConsBasicLinear(_scip,
                                                            &cons,
                                                            oss.str().c_str(),
                                                            0,
                                                            nullptr,
                                                            nullptr,
                                                            -SCIPinfinity(_scip),
                                                            1.));
                    SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _x[i][k], 1.));
                    SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _x[j][k], 1.));

                    if (_allow_violations)
                    {
                        SCIP_CALL_EXC(SCIPaddCoefLinear(_scip, cons, _v[i], -1.));
                    }

                    SCIP_CALL_EXC(SCIPaddCons(_scip, cons));
                    SCIP_CALL_EXC(SCIPreleaseCons(_scip, &cons));
                }
            }
        }
    }

    _solution.initialize(_model);
}

void StackingSCIP::run()
{
    SCIP_CALL_EXC(SCIPsolve(_scip));
}

void StackingSCIP::build_solution()
{
    SCIP_SOL *sol = SCIPgetBestSol(_scip);

    if (sol == nullptr)
    {
        throw std::runtime_error("StackingSCIP::build_solution: no solution found");
    }

    for (std::size_t i = 0; i < _solution.n_items(); ++i)
    {
        for (std::size_t k = 0; k < _solution.n_stacks(); ++k)
        {
            if (SCIPgetSolVal(_scip, sol, _x[i][k]) > 0.5)
            {
                _solution.push(_model, i, k);
            }
        }
    }

    // Check if the objective value matches
    double expected = _solution.n_blocking_items();
    if (_allow_violations)
    {
        expected += _solution.n_items() * _solution.n_violating_items(_model);
    }

    if (std::abs(SCIPgetSolOrigObj(_scip, sol) - expected) > 1e-6)
    {
        throw std::runtime_error("StackingSCIP::build_solution: objective values do not match");
    }

    // Check if the blocking items match
    for (std::size_t i = 0; i < _solution.n_items(); ++i)
    {
        if ((SCIPgetSolVal(_scip, sol, _z[i]) > 0.5) != (_solution.n_items_blocked_by(i) > 0))
        {
            throw std::runtime_error("StackingSCIP::build_solution: blocking items do not match");
        }
    }
}

void StackingSCIP::release()
{
    for (std::size_t i = 0; i < _z.size(); ++i)
    {
        SCIP_CALL_EXC(SCIPreleaseVar(_scip, &_z[i]));
    }

    for (std::size_t i = 0; i < _v.size(); ++i)
    {
        SCIP_CALL_EXC(SCIPreleaseVar(_scip, &_v[i]));
    }

    for (std::size_t i = 0; i < _x.size(); ++i)
    {
        for (std::size_t k = 0; k < _x[i].size(); ++k)
        {
            SCIP_CALL_EXC(SCIPreleaseVar(_scip, &_x[i][k]));
        }
    }

    SCIPfreeProb(_scip);

    _z.clear();
    _v.clear();
    _x.clear();
}

void StackingSCIP::export_model(const char *filename) const
{
    SCIP_CALL_EXC(SCIPwriteOrigProblem(_scip, filename, nullptr, false));
}

void StackingSCIP::export_solutions(const char *filename) const
{
    std::FILE *file;

    file = std::fopen(filename, "w");
    if (file == nullptr)
    {
        std::cerr << "WARNING: StackingSCIP::export_solutions: cannot open file '" << filename << "'" << std::endl;
        return;
    }
    SCIPprintBestSol(_scip, file, false);
    std::fclose(file);
}