/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "stacking_cplex.hpp"
#include <sstream>
#include <stdexcept>

StackingCPLEX::StackingCPLEX()
    : _model(_env),
      _solver(_env)
{
}

IloAlgorithm::Status StackingCPLEX::status() const
{
    return _solver.getStatus();
}

bool StackingCPLEX::is_optimal() const
{
    return _solver.getStatus() == IloAlgorithm::Optimal;
}

bool StackingCPLEX::is_feasible() const
{
    return _solver.getStatus() == IloAlgorithm::Optimal ||
           _solver.getStatus() == IloAlgorithm::Feasible;
}

const StackingCPLEX::Solution &StackingCPLEX::solution() const
{
    return _solution;
}

IloCplex::ParameterSet StackingCPLEX::parameter_set()
{
    return _solver.getParameterSet();
}

void StackingCPLEX::set_parameter_set(IloCplex::ParameterSet set)
{
    _solver.setParameterSet(set);
}

void StackingCPLEX::set_model(const Model &model)
{
    _stacking_model = model;
}

void StackingCPLEX::set_allow_violations(bool allow)
{
    _allow_violations = allow;
}

void StackingCPLEX::set_valid_inequality(bool valid_inequality)
{
    _valid_inequality = valid_inequality;
}

void StackingCPLEX::set_quiet()
{
    _env.setOut(_env.getNullStream());
    _solver.setOut(_env.getNullStream());
}

void StackingCPLEX::set_out_stream(std::ostream &os)
{
    _env.setOut(os);
    _solver.setOut(os);
}

void StackingCPLEX::set_warning_stream(std::ostream &os)
{
    _env.setWarning(os);
    _solver.setWarning(os);
}

void StackingCPLEX::set_error_stream(std::ostream &os)
{
    _env.setError(os);
    _solver.setError(os);
}

void StackingCPLEX::set_debug(bool debug)
{
    _debug = debug;
}

void StackingCPLEX::build_model()
{
    const Model &m = _stacking_model;
    std::size_t n_fix = m.n_initial_items();
    std::size_t n = m.n_items() - n_fix;

    // Initial height after considering initial items
    std::vector<std::size_t> initial_height(m.n_stacks(), 0);

    _model = IloModel(_env);

    // Blocking items variables
    _z = IloNumVarArray(_env, n, 0, 1, IloNumVar::Bool);

    for (std::size_t i = 0; i < n; ++i)
    {
        std::ostringstream oss;
        oss << "z{" << n_fix + i + 1 << "}";
        _z[i].setName(oss.str().c_str());
    }
    _model.add(_z);

    if (_allow_violations)
    {
        // Violating items variables
        _v = IloNumVarArray(_env, n, 0, 1, IloNumVar::Bool);

        for (std::size_t i = 0; i < n; ++i)
        {
            std::ostringstream oss;
            oss << "v{" << n_fix + i + 1 << "}";
            _v[i].setName(oss.str().c_str());
        }
        _model.add(_v);
    }

    // Assignment variables
    _x = IloArray<IloNumVarArray>(_env, n);
    for (std::size_t i = 0; i < n; ++i)
    {
        _x[i] = IloNumVarArray(_env, m.n_stacks(), 0, 1, IloNumVar::Bool);

        for (std::size_t k = 0; k < m.n_stacks(); ++k)
        {
            std::ostringstream oss;
            oss << "x{" << n_fix + i + 1 << "," << k + 1 << "}";
            _x[i][k].setName(oss.str().c_str());
        }
        _model.add(_x[i]);
    }

    // Count the number of fixed blocking/violating items and the initial capacity of stacks
    _n_fixed_blocking_items = 0;
    _n_fixed_violating_items = 0;
    for (std::size_t i = 0; i < n_fix; ++i)
    {
        bool blocking = false;
        bool violating = false;

        for (std::size_t j = 0; j < i && (!blocking || !violating); ++j)
        {
            if (m.initial_position(i) == m.initial_position(j))
            {
                if (m.r(i, j))
                {
                    blocking = true;
                }
                if (!m.s(i, j))
                {
                    violating = true;
                }
            }
        }

        if (blocking)
        {
            ++_n_fixed_blocking_items;
        }
        if (violating)
        {
            ++_n_fixed_violating_items;
        }
        ++initial_height.at(m.initial_position(i));
    }

    if (_allow_violations)
    {
        // Minimize the number of blocking items and a big M penalty on violations
        _objective = IloObjective(_env, IloSum(_z) + m.n_items() * IloSum(_v) + _n_fixed_blocking_items + m.n_items() * _n_fixed_violating_items, IloObjective::Minimize);
        _objective.setName("n_blocking_items_violation");
    }
    else
    {
        // Minimize the number of blocking items
        _objective = IloObjective(_env, IloSum(_z) + _n_fixed_blocking_items, IloObjective::Minimize);
        _objective.setName("n_blocking_items");
    }

    _model.add(_objective);

    // Assignment constraint
    for (std::size_t i = 0; i < n; ++i)
    {
        IloNumExpr expr(_env);
        std::ostringstream oss;

        for (std::size_t k = 0; k < m.n_stacks(); ++k)
        {
            expr += _x[i][k];
        }
        expr = (expr == 1);

        oss << "assign{" << n_fix + i + 1 << "}";
        expr.setName(oss.str().c_str());
        _model.add(expr);
    }

    // Capacity constraint
    if (m.n_items() > m.capacity())
    {
        for (std::size_t k = 0; k < m.n_stacks(); ++k)
        {
            IloNumExpr expr(_env);
            std::ostringstream oss;

            for (std::size_t i = 0; i < n; ++i)
            {
                expr += _x[i][k];
            }
            expr = (expr <= IloNum(int(m.capacity()) - int(initial_height[k])));

            oss << "capacity{" << k + 1 << "}";
            expr.setName(oss.str().c_str());
            _model.add(expr);
        }
    }

    // Soft stacking constraint
    for (std::size_t i = 0; i < n; ++i)
    {
        // Only incoming items need to decide if they are blocking or not
        std::size_t i_ = n_fix + i;

        // Iterate over all items j arriving earlier than item i
        for (std::size_t j_ = 0; j_ < i_; ++j_)
        {
            if ((_allow_violations || m.s(i_, j_)) && m.r(i_, j_))
            {
                if (j_ < n_fix)
                {
                    // Item j is fixed, then we need only one constraint (for the stack where j is placed)
                    std::size_t k = m.initial_position(j_);

                    std::ostringstream oss;
                    oss << "softstack{" << i_ + 1 << "," << j_ + 1 << "," << k + 1 << "}";

                    auto expr = (_x[i][k] <= _z[i]);
                    expr.setName(oss.str().c_str());
                    _model.add(expr);
                }
                else
                {
                    // Both i and j are incoming items, build one constraint per stack
                    for (std::size_t k = 0; k < m.n_stacks(); ++k)
                    {
                        std::ostringstream oss;
                        oss << "softstack{" << i_ + 1 << "," << j_ + 1 << "," << k + 1 << "}";

                        std::size_t j = j_ - n_fix;
                        auto expr = (_x[i][k] + _x[j][k] <= 1 + _z[i]);
                        expr.setName(oss.str().c_str());
                        _model.add(expr);
                    }
                }
            }
        }
    }

    // Hard stacking constraint
    for (std::size_t i = 0; i < n; ++i)
    {
        // Only incoming items need to decide if they are violating or not
        std::size_t i_ = n_fix + i;

        // Iterate over all items j arriving earlier than item i
        for (std::size_t j_ = 0; j_ < i_; ++j_)
        {
            if (!m.s(i_, j_))
            {
                if (j_ < n_fix)
                {
                    // Item j is fixed, then we need only one constraint (for the stack where j is placed)
                    std::size_t k = m.initial_position(j_);

                    if (_allow_violations)
                    {
                        std::ostringstream oss;
                        oss << "softstack{" << i_ + 1 << "," << j_ + 1 << "," << k + 1 << "}";

                        auto expr = (_x[i][k] <= _v[i]);
                        expr.setName(oss.str().c_str());
                        _model.add(expr);
                    }
                    else
                    {
                        // Only fix the variable
                        _x[i][k].setUB(0);
                    }
                }
                else
                {
                    // Both i and j are incoming items, build one constraint per stack
                    for (std::size_t k = 0; k < m.n_stacks(); ++k)
                    {
                        std::ostringstream oss;
                        oss << "hardstack{" << i_ + 1 << "," << j_ + 1 << "," << k + 1 << "}";

                        if (_allow_violations)
                        {
                            std::size_t j = j_ - n_fix;
                            auto expr = (_x[i][k] + _x[j][k] <= 1 + _v[i]);
                            expr.setName(oss.str().c_str());
                            _model.add(expr);
                        }
                        else
                        {
                            std::size_t j = j_ - n_fix;
                            auto expr = (_x[i][k] + _x[j][k] <= 1);
                            expr.setName(oss.str().c_str());
                            _model.add(expr);
                        }
                    }
                }
            }
        }
    }

    _n_valid_inequalities = 0;
    if (_valid_inequality)
    {
        // Valid inequality: Item cannot be blocking if no conflicting item is placed below it
        for (std::size_t i = 0; i < n; ++i)
        {
            std::size_t i_ = n_fix + i;

            for (std::size_t k = 0; k < m.n_stacks(); ++k)
            {
                IloNumExpr expr(_env);
                std::ostringstream oss;
                bool keep = true;

                expr = (_x[i][k] + _z[i]);

                // Iterate over all items j arriving earlier than item i
                for (std::size_t j_ = 0; j_ < i_ && keep; ++j_)
                {
                    if (m.s(i_, j_) && m.r(i_, j_))
                    {
                        if (j_ < n_fix)
                        {
                            keep = false;
                        }
                        else
                        {
                            std::size_t j = j_ - n_fix;
                            expr -= _x[j][k];
                        }
                    }
                }

                // If a fixed item is blocked by i, do not create the inequality
                if (keep)
                {
                    expr = (expr <= 1);
                    oss << "valinq{" << i_ + 1 << "," << k + 1 << "}";
                    expr.setName(oss.str().c_str());
                    _model.add(expr);
                    ++_n_valid_inequalities;
                }
            }
        }
    }

    _solver.extract(_model);
    _solution.initialize(m);
}

void StackingCPLEX::run()
{
    _solver.solve();
}

void StackingCPLEX::build_solution()
{
    std::size_t n_fix = _stacking_model.n_initial_items();
    std::size_t n = _stacking_model.n_items() - n_fix;

    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t k = 0; k < _solution.n_stacks(); ++k)
        {
            if (_solver.getValue(_x[i][k]) > 0.5)
            {
                // Items in the CPLEX model are indexed from 0 for the first incoming item
                std::size_t i_ = n_fix + i;
                _solution.push(_stacking_model, i_, k);
            }
        }
    }

    // Check if the objective value matches
    if (is_feasible())
    {
        double expected = _solution.n_blocking_items();
        if (_allow_violations)
        {
            expected += _solution.n_items() * _solution.n_violating_items(_stacking_model);
        }

        if (is_optimal())
        {
            if (std::abs(_solver.getValue(_objective) - expected) > 1e-6)
            {
                throw std::runtime_error("StackingCPLEX::build_solution: objective values do not match");
            }
        }
        else
        {
            // When the branch and bound is not finished,
            // CPLEX may set unnecessary z_i = 1 values,
            // leading to an objective value greater than expected.
            // So instead we check whether the CPLEX objective is less than expected
            if (_solver.getValue(_objective) + 1e-6 < expected)
            {
                throw std::runtime_error("StackingCPLEX::build_solution: invalid objective value");
            }
        }
    }

    // Check if the blocking items match
    for (std::size_t i = 0; i < n; ++i)
    {
        std::size_t i_ = n_fix + i;
        if (is_optimal())
        {
            if ((_solver.getValue(_z[i]) > 0.5) != (_solution.n_items_blocked_by(i_) > 0))
            {
                throw std::runtime_error("StackingCPLEX::build_solution: blocking items do not match");
            }
        }
        else
        {
            if ((_solver.getValue(_z[i]) < 0.5) && (_solution.n_items_blocked_by(i_) > 0))
            {
                throw std::runtime_error("StackingCPLEX::build_solution: invalid blocking items");
            }
        }
    }
}

std::size_t StackingCPLEX::n_valid_inequalities() const
{
    return _n_valid_inequalities;
}

void StackingCPLEX::export_model(const char *filename) const
{
    _solver.exportModel(filename);
}

void StackingCPLEX::export_solutions(const char *filename) const
{
    _solver.writeSolutions(filename);
}