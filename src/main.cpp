/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "command.hpp"
#include "command_cplex.hpp"
#include "command_gurobi.hpp"
#include "command_heuristic.hpp"
#include "command_scip.hpp"
#include "dopt.hpp"
#include "run_info.hpp"
#include <chrono>
#include <fstream>
#include <iostream>
#include <nlohmann/json.hpp>

// Execute a given command line runner with arguments
void run(Command &&cmd, const dopt::Map &args)
{
    StackingModel model;
    std::ifstream instance_file;
    std::chrono::time_point<std::chrono::steady_clock> begin_time, end_time;
    double total_time;
    std::string instance_filename;
    bool print_solution = false;
    bool normalize = false;
    bool debug = false;
    bool verbose = true;

    // Read command line options
    instance_filename = args[1].str();
    print_solution = args["print-solution"];
    normalize = args["normalize"];
    debug = args["debug"];
    verbose = !args["quiet"];
    cmd.read_options(args);

    if (verbose)
    {
        // Print command line options
        std::clog << "Options:" << std::endl
                  << "  instance_filename = '" << instance_filename << "'" << std::endl;
        cmd.print_options(std::clog);
        std::clog << "  print_solution    = " << print_solution << std::endl
                  << "  normalize         = " << normalize << std::endl
                  << "  debug             = " << debug << std::endl
                  << std::endl;
    }

    // Read the instance
    if (verbose)
    {
        std::clog << "> Read instance..." << std::endl;
    }
    instance_file.open(instance_filename);
    if (!instance_file.is_open())
    {
        throw std::runtime_error("main: cannot open file");
    }
    model.read(instance_file);
    instance_file.close();

    // Initialize (read the instance)
    if (verbose)
    {
        std::clog << "> Initialize..." << std::endl;
    }
    cmd.initialize(model);

    // Solve (run the algorithm)
    if (verbose)
    {
        std::clog << "> Solve..." << std::endl;
    }
    begin_time = std::chrono::steady_clock::now();
    cmd.solve();
    end_time = std::chrono::steady_clock::now();

    // Get computational time
    total_time = std::chrono::duration<double>(end_time - begin_time).count();

    // Finalize (convert the best solution)
    if (verbose)
    {
        std::clog << "> Finalize..." << std::endl;
    }
    auto solution = cmd.finalize();

    // Check solution validity
    if (verbose)
    {
        std::clog << "> Check..." << std::endl;
    }
    solution.check(model);

    // Normalize the solution if requested
    if (normalize && solution.is_feasible(model))
    {
        if (debug)
        {
            std::clog << std::endl
                      << "Solution is " << (solution.is_feasible(model) ? "feasible" : "infeasible") << std::endl
                      << "# blocking items: " << solution.n_blocking_items() << std::endl
                      << "# blocked items:  " << solution.n_blocked_items() << std::endl
                      << std::endl;
            solution.print_layout(std::clog);
            std::clog << std::endl;
        }

        // Normalize
        if (verbose)
        {
            std::clog << "> Normalize..." << std::endl;
        }
        solution.normalize();

        // Check solution validity once again
        if (verbose)
        {
            std::clog << "> Check..." << std::endl;
        }
        solution.check(model);
    }

    // Display information in verbose mode
    if (verbose)
    {
        if (debug && !model.is_arbitrary())
        {
            std::clog << std::endl
                      << "Due times:" << std::endl;
            solution.print_layout_d(model, std::clog);
            std::clog << std::endl
                      << "Weights:" << std::endl;
            solution.print_layout_w(model, std::clog);
        }
        std::clog << std::endl;
        solution.print_layout(std::clog);
        std::clog << std::endl;

        std::clog << "Solution is " << (solution.is_feasible(model) ? "feasible" : "infeasible") << std::endl
                  << "# blocking items: " << solution.n_blocking_items() << std::endl
                  << "# blocked items:  " << solution.n_blocked_items() << std::endl
                  << "Total time: " << total_time << " s" << std::endl
                  << std::endl;
    }

    // Output data
    nlohmann::json data = nlohmann::json::object();

    data["is_feasible"] = solution.is_feasible(model);
    data["n_blocking_items"] = solution.n_blocking_items();
    data["n_blocked_items"] = solution.n_blocked_items();
    data["total_time"] = total_time;

    if (print_solution)
    {
        data["solution"] = nlohmann::json::array();
        for (auto k : solution.assignments())
        {
            data["solution"].push_back(k + 1);
        }
    }

    // Add solver-specific data
    cmd.output(data, solution);

    if (verbose)
    {
        std::cout << data.dump(2) << std::endl;
    }
    else
    {
        std::cout << data << std::endl;
    }
}

int main(int argc, char *argv[])
{
    using arg = dopt::Argument;

    dopt::ParserArgp parse{
        "cplex <instance>\n"
        "gurobi <instance>\n"
        "heuristic <instance>\n"
        "info <instance>\n"
        "scip <instance>",

        "Solver for stacking problems.",
        {{"CPLEX/GUROBI/SCIP options:"},
         {{"time-limit"}, /*      */ arg("<s>"), "Set the maximum time, in seconds"},
         {{"memory-limit"}, /*   */ arg("<mb>"), "Set the maximum memory size, in megabytes"},
         {{"feasibility-only"}, /*            */ "Stop at the first feasible solution"},
         {{"valid-inequality"}, /*            */ "Enable valid inequalities"},
         {{"export-log"}, /*      */ arg("<f>"), "Output the log to a file"},
         {{"export-model"}, /*    */ arg("<f>"), "Output the model to a file"},
         {{"export-solutions"}, /**/ arg("<f>"), "Output the solutions to a file"},

         {"Heuristic options:"},
         {{"n-iterations"}, /*      */ arg("<n>"), "Number of iterations [default: 1]"},
         {{"sort"}, /*              */ arg("<s>"), "Sort rule (degree, lifo, fifo) [default: degree]"},
         {{"randomness"}, /*        */ arg("<q>"), "Randomness parameter [default: 0.1]"},
         {{"extended-construct"}, /*            */ "Enable extended construction"},
         {{"no-repair"}, /*                     */ "Do not attempt to repair infeasible solutions"},
         {{"local-search-depth"}, /**/ arg("<k>"), "Local search depth (k-opt) [default: 2]"},
         {{"no-extended-local-search"}, /*      */ "Disable extended local search"},
         {{"only-exchange"}, /*                 */ "Restrict 2-opt local search to exchanges"},

         {"Info options:"},
         {{"export-s-graph"}, /*    */ arg("<f>"), "Output the hard conflict graph to a file"},
         {{"export-r-graph"}, /*    */ arg("<f>"), "Output the soft conflict graph to a file"},
         {{"export-c-graph"}, /*    */ arg("<f>"), "Output the combined conflict graph to a file"},

         {"Other options:"},
         {{"allow-violations"}, /**/ "Allow hard constraint violations"},
         {{"print-solution"}, /*  */ "Print the solution in the JSON"},
         {{"normalize"}, /*       */ "Normalize the final stacking layout"},
         {{"seed"}, /**/ arg("<s>"), "Specify random seed"},
         {{"debug"}, /*           */ "Print debug information"},
         {{'q', "quiet"}, /*      */ "Print only minimum information"}}};

    auto args = parse(argc, argv);

    if (args.size() == 0)
    {
        std::cerr << "Please specify a command" << std::endl;
        return 0;
    }
    else if (args.size() == 1)
    {
        std::cerr << "Please specify a filename" << std::endl;
        return 0;
    }

    auto command = args[0].str();

    if (command == "info")
    {
        run_info(args);
    }
    else if (command == "cplex")
    {
        run(CommandCPLEX(), args);
    }
    else if (command == "gurobi")
    {
        run(CommandGUROBI(), args);
    }
    else if (command == "heuristic")
    {
        run(CommandHeuristic(), args);
    }
    else if (command == "scip")
    {
        run(CommandSCIP(), args);
    }

    return 0;
}