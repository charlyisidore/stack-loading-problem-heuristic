/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "command_scip.hpp"

CommandSCIP::~CommandSCIP()
{
    // Free memory
    solver.release();
}

void CommandSCIP::read_options(const dopt::Map &args)
{
    allow_violations = args["allow-violations"];
    if (args["time-limit"])
    {
        time_limit = args["time-limit"].get<unsigned int>();
    }
    if (args["memory-limit"])
    {
        memory_limit = args["memory-limit"].get<unsigned int>();
    }
    if (args["export-log"])
    {
        log_filename = args["export-log"].str();
    }
    if (args["export-model"])
    {
        model_filename = args["export-model"].str();
    }
    if (args["export-solutions"])
    {
        sol_filename = args["export-solutions"].str();
    }
    debug = args["debug"];
    verbose = !args["quiet"];
}

void CommandSCIP::print_options(std::ostream &os) const
{
    os << "  allow_violations  = " << allow_violations << std::endl
       << "  time_limit        = ";
    if (time_limit > 0)
    {
        os << time_limit << " s";
    }
    else
    {
        os << "inf";
    }
    os << std::endl
       << "  memory_limit      = ";
    if (memory_limit > 0)
    {
        os << memory_limit << " MB";
    }
    else
    {
        os << "inf";
    }
    os << std::endl
       << "  log_filename      = '" << log_filename << "'" << std::endl
       << "  model_filename    = '" << model_filename << "'" << std::endl
       << "  sol_filename      = '" << sol_filename << "'" << std::endl;
}

void CommandSCIP::initialize(const StackingModel &m)
{
    model = m;

    // Write the SCIP log in a file
    if (!log_filename.empty())
    {
        solver.set_log_file(log_filename.c_str());
    }
    if (!verbose)
    {
        solver.set_quiet();
    }
    solver.set_debug(debug);

    // Set SCIP parameters
    if (time_limit > 0)
    {
        solver.set_real_param("limits/time", time_limit);
    }
    if (memory_limit > 0)
    {
        solver.set_real_param("limits/memory", memory_limit);
    }

    // Determine if stacking violations are allowed
    solver.set_allow_violations(allow_violations);

    // Convert the stacking model to a MIP model
    solver.set_model(model);
    solver.build_model();

    // Export model if requested
    if (!model_filename.empty())
    {
        if (verbose)
        {
            std::clog << "> Export model to '" << model_filename << "'..." << std::endl;
        }
        solver.export_model(model_filename.c_str());
    }
}

void CommandSCIP::solve()
{
    solver.run();
}

StackingSolution CommandSCIP::finalize()
{
    if (verbose && solver.is_optimal())
    {
        std::clog << "Solution is optimal" << std::endl;
    }

    // Export solutions if requested
    if (!sol_filename.empty())
    {
        if (verbose)
        {
            std::clog << "> Export solutions to '" << sol_filename << "'..." << std::endl;
        }
        solver.export_solutions(sol_filename.c_str());
    }

    // Convert the MIP solution to a stacking solution
    if (solver.is_feasible())
    {
        solver.build_solution();
    }

    return solver.solution();
}

void CommandSCIP::output(nlohmann::json &data, [[maybe_unused]] const StackingSolution &solution) const
{
    data["is_optimal"] = solver.is_optimal();
}