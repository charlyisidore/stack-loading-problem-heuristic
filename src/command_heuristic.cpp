/* -*-c++-*-
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "command_heuristic.hpp"
#include <chrono>

CommandHeuristic::CommandHeuristic()
    : solver(rng)
{
}

void CommandHeuristic::read_options(const dopt::Map &args)
{
    if (args["n-iterations"])
    {
        n_iterations = args["n-iterations"].get<unsigned int>();
    }
    if (args["sort"])
    {
        sort_name = args["sort"].str();
    }
    if (args["randomness"])
    {
        randomness = args["randomness"].get<double>();
    }
    extended_construct = args["extended-construct"];
    repair = !args["no-repair"];

    if (args["local-search-depth"])
    {
        local_search_depth = args["local-search-depth"].get<unsigned int>();
    }
    extended_local_search = !args["no-extended-local-search"];
    only_exchange = args["only-exchange"];
    allow_violations = args["allow-violations"];
    if (args["seed"])
    {
        seed = args["seed"].get<unsigned int>();
    }
    else
    {
        seed = std::random_device{}();
    }
    debug = args["debug"];
    verbose = !args["quiet"];
}

void CommandHeuristic::print_options(std::ostream &os) const
{
    os << "  n_iterations          = " << n_iterations << std::endl
       << "  sort_name             = '" << sort_name << "'" << std::endl
       << "  randomness            = " << randomness << std::endl
       << "  extended_construct    = " << extended_construct << std::endl
       << "  repair                = " << repair << std::endl
       << "  local_search_depth    = " << local_search_depth << std::endl
       << "  extended_local_search = " << extended_local_search << std::endl
       << "  only_exchange         = " << only_exchange << std::endl
       << "  allow_violations      = " << allow_violations << std::endl
       << "  seed              = " << seed << std::endl;
}

void CommandHeuristic::initialize(const StackingModel &m)
{
    model = m;

    // Random number generator init
    rng.seed(seed);

    solver.set_debug(debug);
    solver.set_model(model);

    if (sort_name == "degree")
    {
        solver.set_sort_rule(StackingHeuristic::DegreeSort(model));
    }
    else if (sort_name == "lifo")
    {
        solver.set_sort_rule(StackingHeuristic::LIFOSort());
    }
    else if (sort_name == "fifo")
    {
        solver.set_sort_rule(StackingHeuristic::FIFOSort());
    }
    else
    {
        throw std::runtime_error("main: unknown sort rule");
    }

    // We set allow_violations to true at construction level only if repair is disabled
    // If repair is enabled => allow violations after repair
    // If repair is disabled => allow violations at construction step
    solver.set_select_rule(StackingHeuristic::GeoSelect(rng, randomness, extended_construct, allow_violations && !repair, debug));
    solver.set_repair(repair);
    solver.set_allow_violations(allow_violations);
}

void CommandHeuristic::solve()
{
    bool best_solution_is_feasible = false;
    bool best_solution_is_optimal = false;

    n_blocking_items_construct = model.n_items();
    n_blocked_items_construct = model.n_items();
    construct_time = 0;
    local_search_time = 0;

    for (std::size_t iteration = 0; iteration < n_iterations && !best_solution_is_optimal; ++iteration)
    {
        std::chrono::time_point<std::chrono::steady_clock> begin_time, end_construct_time, end_time;

        begin_time = std::chrono::steady_clock::now();

        if (verbose)
        {
            std::clog << "= Iteration " << iteration + 1 << "/" << n_iterations << std::endl
                      << "> Construction..." << std::endl;
        }
        solver.construct();

        if (debug)
        {
            std::clog << std::endl
                      << "Solution is " << (solver.solution().is_feasible(model) ? "feasible" : "infeasible") << std::endl
                      << "# blocking items: " << solver.solution().n_blocking_items() << std::endl
                      << "# blocked items:  " << solver.solution().n_blocked_items() << std::endl
                      << std::endl;
            solver.solution().print_layout(std::clog);
            std::clog << std::endl;
            // Will throw an exception in case of failure
            solver.solution().check(model);
        }

        end_construct_time = std::chrono::steady_clock::now();

        bool feasible = solver.solution().is_feasible(model);
        bool optimal = (feasible && solver.solution().n_blocking_items() == 0);

        // Save objective value of constructed solution
        if (feasible &&
            (solver.solution().n_blocking_items() < n_blocking_items_construct ||
             (solver.solution().n_blocking_items() == n_blocking_items_construct &&
              solver.solution().n_blocked_items() < n_blocked_items_construct)))
        {
            n_blocking_items_construct = solver.solution().n_blocking_items();
            n_blocked_items_construct = solver.solution().n_blocked_items();
        }

        // Local search
        if (feasible && !optimal)
        {
            if (verbose)
            {
                std::clog << "> Local search..." << std::endl;
            }

            while (solver.local_search(local_search_depth, extended_local_search, only_exchange))
            {
            }

            if (debug)
            {
                std::clog << std::endl
                          << "# blocking items: " << solver.solution().n_blocking_items() << std::endl
                          << "# blocked items:  " << solver.solution().n_blocked_items() << std::endl
                          << std::endl;
                solver.solution().print_layout(std::clog);
                std::clog << std::endl;
                // Will throw an exception in case of failure
                solver.solution().check(model);
            }
        }
        else if (verbose)
        {
            if (!feasible)
            {
                std::clog << "Solution is infeasible" << std::endl;
            }
            else if (optimal)
            {
                std::clog << "Solution is optimal" << std::endl;
            }
        }

        end_time = std::chrono::steady_clock::now();

        construct_time += std::chrono::duration<double>(end_construct_time - begin_time).count();
        local_search_time += std::chrono::duration<double>(end_time - end_construct_time).count();

        if (feasible &&
            (!best_solution_is_feasible ||
             ((
                 solver.solution().n_blocking_items() < best_solution.n_blocking_items() ||
                 (solver.solution().n_blocking_items() == best_solution.n_blocking_items() &&
                  solver.solution().n_blocked_items() < best_solution.n_blocked_items())))))
        {
            best_solution = solver.solution();
            best_solution_is_feasible = true;

            if (optimal)
            {
                best_solution_is_optimal = true;
            }

            if (verbose)
            {
                std::clog << "Best solution" << std::endl
                          << "# blocking items: " << best_solution.n_blocking_items() << std::endl
                          << "# blocked items:  " << best_solution.n_blocked_items() << std::endl;
            }
        }
        else if (!best_solution_is_feasible)
        {
            best_solution = solver.solution();
        }
    }
}

StackingSolution CommandHeuristic::finalize()
{
    return best_solution;
}

void CommandHeuristic::output(nlohmann::json &data, [[maybe_unused]] const StackingSolution &solution) const
{
    data["seed"] = seed;
    data["n_blocking_items_construct"] = n_blocking_items_construct;
    data["n_blocked_items_construct"] = n_blocked_items_construct;
    data["construct_time"] = construct_time;
    data["local_search_time"] = local_search_time;
    data["n_repairs"] = solver.n_repairs();
    data["n_1_opt_moves"] = solver.n_1_opt_moves();
    data["n_2_opt_moves"] = solver.n_2_opt_moves();
}
